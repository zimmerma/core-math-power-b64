#!/usr/bin/env bash

MAKE=make

FUN="${!#}"
ARGS=("${@:1:$#-1}")

FILE="$(echo src/*/*/"$FUN".c)"
ORIG_DIR="$(dirname "$FILE")"

if ! [ -d "$ORIG_DIR" ]; then
    echo "Could not find $FUN"
    exit 1
fi

if [ "$CFLAGS" == "" ]; then
   export CFLAGS="-O3 -march=native -fno-finite-math-only -frounding-math -fsignaling-nans"
fi

# default for POW_ITERATION is 15 (all paths enabled)
export POW_ITERATION=${POW_ITERATION:-15}

# define CORE_MATH_NO_OPENMP if you don't want OpenMP
if [[ -z "$CORE_MATH_NO_OPENMP" ]]; then
   OPENMP=-fopenmp
else
   OPENMP=
fi

TMP_DIR="$(mktemp -d --tmpdir core-math.XXXXXX)"

trap 'rm -rf "$TMP_DIR"' EXIT

DIR="$TMP_DIR/toto/$(basename "$ORIG_DIR")"

has_symbol () {
    [ "$(nm "$LIBM" | while read a b c; do if [ "$c" = "$FUN" ]; then echo OK; return; fi; done | wc -l)" -ge 1 ]
}

if [[ -n "$LIBM" ]] && ! has_symbol; then
    echo "Error: symbol $FUN is not present in $LIBM" >&2
    exit 2
fi

mkdir "$TMP_DIR/toto"
cp -a "$ORIG_DIR" "$ORIG_DIR/../support" "$TMP_DIR/toto"
cp -a "$ORIG_DIR/../../generic" "$TMP_DIR"

if [ -n "${ARGS[0]}" ]; then
    KIND="${ARGS[0]}"
    ARGS=("${ARGS[@]:1}")
else
    SIZE=${FILE#src/binary}
    SIZE=${SIZE%%/*}
    case "$SIZE" in
        32)
            KIND=--exhaustive
            ;;
        *)
            KIND=--worst
    esac
fi

case "$KIND" in
    --exhaustive)
        "$MAKE" --quiet -C "$DIR" clean
        "$MAKE" --quiet -C "$DIR" check_exhaustive
        if [ "${#ARGS[@]}" -eq 0 ]; then
            MODES=("--rndn" "--rndz" "--rndu" "--rndd")
        else
            MODES=("${ARGS[@]}")
        fi
        for MODE in "${MODES[@]}"; do
            echo "Running exhaustive check in $MODE mode..."
            $CORE_MATH_LAUNCHER "$DIR/check_exhaustive" "$MODE"
        done
        ;;
    --worst)
        "$MAKE" --quiet -C "$DIR" clean
        "$MAKE" --quiet -C "$DIR" check_worst
        if [ "${#ARGS[@]}" -eq 0 ]; then
            MODES=("--rndn" "--rndz" "--rndu" "--rndd")
        else
            MODES=("${ARGS[@]}")
        fi
        for MODE in "${MODES[@]}"; do
            echo "Running worst cases check in $MODE mode..."
            OPENMP=$OPENMP $CORE_MATH_LAUNCHER "$DIR/check_worst" "$MODE" < "${FILE%.c}.wc"
        done
        ;;
    --special)
        "$MAKE" --quiet -C "$DIR" clean
        "$MAKE" --quiet -C "$DIR" check_special
        if [ "${#ARGS[@]}" -eq 0 ]; then
            MODES=("--rndn" "--rndz" "--rndu" "--rndd")
        else
            MODES=("${ARGS[@]}")
        fi
        for MODE in "${MODES[@]}"; do
            echo "Running special checks in $MODE mode..."
            $CORE_MATH_LAUNCHER "$DIR/check_special" "$MODE"
        done
        ;;
    *)
        echo "Unrecognized command"
        exit 1
esac
