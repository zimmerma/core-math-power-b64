/* Check correctness of bivariate binary32 function on worst cases.

Copyright (c) 2022 Stéphane Glondu, Inria.

This file is part of the CORE-MATH project
(https://core-math.gitlabpages.inria.fr/).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#define _POSIX_C_SOURCE 200809L /* for getline */

#include <fenv.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "function_under_test.h"

typedef union {
  double f;
  uint64_t u;
} f64_u;

static inline int compare(double z1, double z2) {
  if (__builtin_isnan(z1) && __builtin_isnan(z2)) {
    // Two resulting NaNs are equal if they are of the same type (quiet/signaling)
    // and have the same payload
    f64_u _z1 = {.f = z1};
    f64_u _z2 = {.f = z2};

    return (~0ul >> 12) & (_z1.u ^ _z2.u);
  }

  return z1 != z2;
}

double cr_function_under_test(double, double);
double ref_function_under_test(double, double);
int ref_fesetround(int);
void ref_init(void);

int rnd1[] = {FE_TONEAREST, FE_TOWARDZERO, FE_UPWARD, FE_DOWNWARD};

int rnd = 0;

void doloop(void) {
  char *buf = NULL;
  size_t buflength = 0;
  ssize_t n;
  double x, y, z1, z2;
  int count = 0, failures = 0;

  ref_init();
  ref_fesetround(rnd);

  while ((n = getline(&buf, &buflength, stdin)) >= 0) {
    if (n > 0 && buf[0] == '#')
      continue;
    if (sscanf(buf, "%la,%la", &x, &y) == 2) {
      z1 = ref_function_under_test(x, y);
      fesetround(rnd1[rnd]);
      z2 = cr_function_under_test(x, y);
      if (compare(z1, z2)) {
        printf("FAIL x=%la y=%la ref=%la z=%la\n", x, y, z1, z2);
        fflush(stdout);
#ifdef DO_NOT_ABORT
      failures ++;
#else
        exit(1);
#endif
      }
      count++;
    }
  }
  free(buf);

  printf("%d tests passed, %d failures\n", count, failures);
}

int main(int argc, char *argv[]) {
  while (argc >= 2) {
    if (strcmp(argv[1], "--rndn") == 0) {
      rnd = 0;
      argc--;
      argv++;
    } else if (strcmp(argv[1], "--rndz") == 0) {
      rnd = 1;
      argc--;
      argv++;
    } else if (strcmp(argv[1], "--rndu") == 0) {
      rnd = 2;
      argc--;
      argv++;
    } else if (strcmp(argv[1], "--rndd") == 0) {
      rnd = 3;
      argc--;
      argv++;
    } else {
      fprintf(stderr, "Error, unknown option %s\n", argv[1]);
      exit(1);
    }
  }

  doloop();
}
