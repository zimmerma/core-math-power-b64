/* Count/print non-trivial exact and midpoint cases for binary64 power function.

Copyright (c) 2022 Paul Zimmermann, Inria, Tom Hubrecht, ENS.

This file is part of the CORE-MATH project
(https://core-math.gitlabpages.inria.fr/).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/* Exact cases are such that x^y is exactly representable in binary64.
   Midpoint cases are such that x^y is exactly in the middle of two
   representable binary64 numbers: the smallest possible value of x^y is 2^-1075
   (middle of 0 and 2^-1074), and the maximal one is 2^1024*(1-2^-53) (middle of
   DBL_MAX and 2^1024, which is not representable, but this would be a
   hard-to-round case for rounding to nearest). We don't count/print trivial
   solutions: |x|=1, y=0, y=1, y=0.5.

   $ gcc -W -Wall exact.c -lm -lmpfr
   $ ./a.out # count all exact and midpoint cases
   ...
   # count=1071899
   $ ./a.out -mid 0 # count only exact cases
   ...
   # count=842073
*/

#include <assert.h>
#include <math.h>
#include <mpfr.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int print = 0; /* by default, we only count exact and midpoint cases */
int mid = 1;   /* by default, we count or print midpoint cases */
unsigned long count = 0; /* number of solutions */
int check = 0;           /* check solutions */
int subnormal = 1;       /* allow subnormal numbers */

int e_max = 1023;
int e_min = 1074;

static void print_sol(double x, double y) {
  if (check) {
    mpfr_t xx, yy, zz;
    int ret;
    mpfr_init2(xx, 53);
    mpfr_init2(yy, 53);
    mpfr_init2(zz, 53 + mid);
    ret = mpfr_set_d(xx, x, MPFR_RNDN);
    assert(ret == 0);
    ret = mpfr_set_d(yy, y, MPFR_RNDN);
    assert(ret == 0);
    ret = mpfr_pow(zz, xx, yy, MPFR_RNDN);
    assert(ret == 0);
    mpfr_exp_t e = mpfr_get_exp(zz);
    assert(e <= e_max + 1);
    /* smallest normal is 2^-1024, which has MPFR exponent -1023 */
    if (e < -(e_max)) {
      /* check zz is an exact multiple of 2^(-149-mid) */
      mpfr_mul_2exp(zz, zz, (e_min + 1) + mid, MPFR_RNDN);
      assert(mpfr_integer_p(zz));
    }
    mpfr_clear(xx);
    mpfr_clear(yy);
    mpfr_clear(zz);
  }
  count++;
  if (print)
    printf("%la,%la\n", x, y);
}

/* y positive integer: both x and -x are solutions */
static void count_uint_y(int y) {
  double xmin, xmax;
  int emin, emax;
  mpfr_t z;
  unsigned long local_count = 0;
  mpfr_init2(z, 54);
  mpfr_set_ui_2exp(z, 1, -(e_min + 1), MPFR_RNDN);
  mpfr_rootn_ui(z, z, y, MPFR_RNDU);
  xmin = mpfr_get_d(z, MPFR_RNDU);
  mpfr_set_ui_2exp(z, 1, (e_max + 1), MPFR_RNDN);
  mpfr_nextbelow(z);
  mpfr_rootn_ui(z, z, y, MPFR_RNDD);
  xmax = mpfr_get_d(z, MPFR_RNDD);
  /* we want xmin <= x <= xmax, with x^y exact */
  /* write x = m*2^e with m odd, m >= 3 */
  /* compute the maximum odd m such that m^y < 2^(53+mid) */
  mpfr_set_ui_2exp(z, 1, 53 + mid, MPFR_RNDN);
  mpfr_nextbelow(z);
  mpfr_rootn_ui(z, z, y, MPFR_RNDD);
  assert(mpfr_fits_slong_p(z, MPFR_RNDD));
  int maxm = mpfr_get_ui(z, MPFR_RNDD);
  /* since m*2^e <= xmax, we have 2^e <= xmax/m <= xmax/3 */
  mpfr_set_d(z, xmax, MPFR_RNDD);
  mpfr_div_ui(z, z, 3, MPFR_RNDD);
  emax = mpfr_get_exp(z) - 1;
  /* for the minimal exponent, since x is an odd multiple of 2^e,
     x^y is an odd multiple of 2^(y*e), thus we want y*e >= -149-mid */
  emin = -((e_min + mid) / y);
  for (int e = emin; e <= emax; e++)
    for (int m = 3; m <= maxm; m += 2) {
      mpfr_set_ui_2exp(z, m, e, MPFR_RNDN);
      if (mpfr_cmp_d(z, xmin) >= 0 && mpfr_cmp_d(z, xmax) <= 0) {
        double x = mpfr_get_d(z, MPFR_RNDN);
        print_sol(x, (double)y);
        local_count++;
        print_sol(-x, (double)y);
        local_count++;
      }
    }
  mpfr_clear(z);

  if (local_count)
    printf("# y=%d: %lu\n", y, local_count);
}

/* y = n/2^f */
static void count_uint_2exp_y(long n, long f) {
  long F = 1 << f;
  double xmin, xmax;
  long emin, emax;
  mpfr_t z;
  unsigned long local_count = 0;
  double y = ldexp(n, -f);
  mpfr_init2(z, 54);
  mpfr_set_ui_2exp(z, 1, -(e_min + 1), MPFR_RNDN);
  mpfr_rootn_ui(z, z, n, MPFR_RNDU);
  mpfr_pow_ui(z, z, F, MPFR_RNDU);
  xmin = mpfr_get_d(z, MPFR_RNDU);
  mpfr_set_ui_2exp(z, 1, (e_max + 1), MPFR_RNDN);
  mpfr_nextbelow(z);
  mpfr_rootn_ui(z, z, n, MPFR_RNDD);
  mpfr_pow_ui(z, z, F, MPFR_RNDD);
  xmax = mpfr_get_d(z, MPFR_RNDD);
  /* we want xmin <= x <= xmax, with x^y exact */
  /* write x = m*2^e with m odd, m >= 3, m = k^(2^f) */
  /* we should have both k^(2^f) < 2^24 and k^n < 2^(24+mid) */
  /* compute the maximum odd k such that k^(2^f) < 2^24 */
  mpfr_set_ui_2exp(z, 1, 53, MPFR_RNDN);
  mpfr_nextbelow(z);
  mpfr_rootn_ui(z, z, F, MPFR_RNDD);
  assert(mpfr_fits_slong_p(z, MPFR_RNDD));
  long maxk = mpfr_get_ui(z, MPFR_RNDD);
  /* compute the maximum odd k such that k^n < 2^(24+mid) */
  mpfr_set_ui_2exp(z, 1, 53 + mid, MPFR_RNDN);
  mpfr_nextbelow(z);
  mpfr_rootn_ui(z, z, n, MPFR_RNDD);
  assert(mpfr_fits_slong_p(z, MPFR_RNDD));
  long maxk2 = mpfr_get_ui(z, MPFR_RNDD);
  maxk = (maxk < maxk2) ? maxk : maxk2;
  /* Write x=m*2^e, we should have m=k^(2^f) and e multiple of 2^f,
     then x^y = k^n*2^(e*n/2^f).
     We should have m=k^(2^f) with k <= kmax. */
  /* since m*2^e <= xmax, we have 2^e <= xmax/m <= xmax/3 */
  mpfr_set_d(z, xmax, MPFR_RNDD);
  mpfr_div_ui(z, z, 3, MPFR_RNDD);
  emax = mpfr_get_exp(z) - 1;
  /* for the minimal exponent, since x is an odd multiple of 2^e,
     x^y is an odd multiple of 2^(y*e), thus we want e >= -149 and y*e >=
     -149-mid */
  emin = -((e_min + mid) / y);
  if (emin < -e_min)
    emin = -e_min; /* so that x is representable */
  /* we should have e multiple of F */
  while (emin % F)
    emin++;
  for (long e = emin; e <= emax; e += F) {
    for (long k = 3; k <= maxk; k += 2) {
      unsigned long m = k;
      for (long j = 0; j < f; j++)
        m = m * m;
      /* m (odd) should be less than 2^24 */
      assert(m < 0x40000000000000);
      mpfr_set_ui_2exp(z, m, e, MPFR_RNDN);
      if (mpfr_cmp_d(z, xmin) >= 0 && mpfr_cmp_d(z, xmax) <= 0) {
        print_sol(mpfr_get_d(z, MPFR_RNDN), y);
        local_count++;
      }
    }
  }
  mpfr_clear(z);

  if (local_count)
    printf("# y=%a: %lu\n", y, local_count);
}

/* x = +/-2^e:
   for y integer, and -149-mid <= e*y <= 127, both x=2^e and -2^e are solutions
   for y=n/2^f for f>=1, n odd, we need e divisible by 2^f, and -149-mid <=
   e*y/2^f <= 127 */
static void count_pow2_x(long e) {
  unsigned long local_count = 0;
  if (e == 0) /* trivial solutions */
    return;

  /* case y integer */
  double x = ldexp(1.0, e);
  long ymin, ymax;
  if (e > 0) {
    ymin = -((e_min + mid) / e);
    ymax = e_max / e;
  } else /* e < 0 */
  {
    ymin = -(e_max / -e);
    ymax = (e_min + mid) / (-e);
  }
  for (long y = ymin; y <= ymax; y++)
    if (y != 0 && y != 1) {
      print_sol(x, (double)y);
      local_count++;
      print_sol(-x, (double)y);
      local_count++;
    }

  /* case y = n/2^f */
  int f = 1;
  while ((e % 2) == 0) {
    /* invariant: e = e_orig/2^f */
    e = e / 2;
    /* -149-mid <= e*y <= 127 */
    if (e > 0) {
      ymin = -((e_min + mid) / e);
      ymax = e_max / e;
    } else /* e < 0 */
    {
      ymin = -(e_max / -e);
      ymax = (e_min + mid) / (-e);
    }
    /* y should be odd */
    if ((ymin % 2) == 0)
      ymin++;
    for (long y = ymin; y <= ymax; y += 2) {
      print_sol(x, ldexp(y, -f));
      local_count++;
    }
    f++;
  }

  if (local_count)
    printf("# x=2^%ld: %lu\n", e, local_count);
}

int main(int argc, char *argv[]) {
  while (argc >= 2 && argv[1][0] == '-') {
    if (strcmp(argv[1], "-print") == 0) {
      print = 1;
      argv++;
      argc--;
    } else if (strcmp(argv[1], "-check") == 0) {
      check = 1;
      argv++;
      argc--;
    } else if (argc >= 3 && strcmp(argv[1], "-mid") == 0) {
      mid = atoi(argv[2]);
      argv += 2;
      argc -= 2;
    } else if (strcmp(argv[1], "-nosubnormal") == 0) {
      e_min = e_max + 1;
      argv++;
      argc--;
    } else {
      fprintf(stderr, "Error, unknown option %s\n", argv[1]);
      exit(1);
    }
  }

  /* First deal with integer y >= 2. If x is not a power of 2, then y <= 35
     whatever the value of mid, since 3^34 has 54 bits, and 3^35 has 56 bits.
     Indeed, assume x = m*2^e with m odd, then m >= 3, thus we should have
     m^y < 2^(35+mid). */
  for (int n = 5; n <= 34; n++)
    count_uint_y(n);
  /* Now deal with y = n/2^f for a positive integer f, and an odd n. If x is not
     a power of 2, assume x = m*2^e with m odd, m >= 3. Then m^(1/2^f) should be
     an odd integer >= 3, which implies f <= 5 whatever the value of mid, since
     3^(2^5) has 51 bits, and 3^(2^6) has 102 bits.
     For the same reason as above, n <= 35. */
  for (int f = 1; f <= 5; f++)
    for (int n = 1; n <= 33; n += 2)
      if (f * n - 1)
        count_uint_2exp_y(n, f);
  /* Now deal with x=2^e. */
  for (int e = -e_min; e <= e_max; e++)
    count_pow2_x(e);

  printf("# count=%lu\n", count);
}
