# change 'ne' to 'zr', 'up' or 'dn' for the other rounding modes
@rnd = float<ieee_64,ne>;

z rnd= zh + zl;
Z = zh + zl;

Q4 = 0x1.5555555693f4p-5;
Q3 = 0x1.555555572084bp-3;
Q2 = 0x1p-1;
Q1 = 0x1p0;

# z = zh + zl
errz = z - (zh + zl);


# q = __builtin_fma (Q_1[4], zh, Q_1[3])
q rnd= fma (Q4, zh, Q3);
err1a = q - (Q4 * zh + Q3); # error in __builtin_fma (Q_1[4], zh, Q_1[3])
err1b = Q4*zl;              # neglected term Q4*zl
err1 = (err1a + err1b) * Z * Z * Z;

# q = __builtin_fma (q, z, Q_1[2])
q2 rnd= fma (q, z, Q2);
err2a = q2 - (q * z + Q2); # error in __builtin_fma (q, z, Q_1[2])
err2b = q * errz;          # error in z multiplied by q
err2 = (err2a + err2b) * Z * Z;

# fast_two_sum (hi, lo, Q_1[1], q2 * z)
qz rnd= q2 * z;
hi rnd= Q1 + qz;
e rnd= hi - Q1;
lo rnd= qz - e;
err3a = q2 * errz;
err3b = qz - (q2 * z);
err3 = (err3a + err3b + err3c) * Z;

# d_mul (hi2, lo2, zh, zl, hi, lo)
#    a_mul (hi2, s, zh, hi)
#    t = fma (zl, hi, s)
#    lo2 = fma (zh, lo, t)
hi2 rnd= zh * hi;
s = hi2 - (zh * hi);
t rnd= fma (zl, hi, s);
lo2 rnd= fma (zh, lo, t);
err4a = zl * lo;
err4b = t - (zl * hi + s);
err4c = lo2 - (zh * lo + t);
err4 = err4a + err4b + err4c;

# fast_sum (hi3, lo3, Q_1[0], hi2, lo2)
#    fast_two_sum (hi3, lo3, Q_1[0], hi2)
#    hi3 = Q_1[0] + hi2
#    e2 = hi3 - Q_1[0]
#    lo3 = hi2 - e2
#    lo3 += lo2    
hi3 rnd= 1 + hi2;
e2 rnd= hi3 - 1;
lo3_tmp rnd= hi2 - e2;
lo3 rnd= lo3_tmp + lo2;
err5b = lo3 - (lo3_tmp + lo2);
err5 = err5a+err5b;
res = hi3 + lo3;

{
    |err0| <= 0x1.748p-78 ->  # Sollya error < 2^-77.459
    |zh| <= 0x1.651p-14 ->    # |zh| < 2^-13.52
    |zl| <= 0x1.126p-30 ->    # |zl| < 2^-29.9
    |err3c/hi| <= 0x1p-105 -> # error bound in fast_two_sum
    |lo/hi| <= 0x1p-52     -> # fast_two_sum post-condition
    |err5a/hi3| <= 0x1p-105 -> # error bound in fast_two_sum
    |lo3_tmp/hi3| <= 0x1p-52 -> # fast_two_sum post-condition
    |lo3| in ? # low-order term of q_1
}
