#!/bin/bash
# script to check the error bound of the log_1() function
# r is the entry in the _INVERSE[] table
r=$1
# l1 and l2 and the entries in _LOG_INV[]
l1=$2
l2=$3
# t0 is the smallest value of t for this i-value
t0=$4
# t1 is the largest value of t for this i-value
t1=$5
# rnd is the rounding mode (ne for nearest, zr for towards zero, up for upwards, and dn for downwards)
rnd=$6
# E is the exponent
E=$7
sed "s/TEMPR/$r/g" log_1_template.g | sed "s/TEMPL1/$l1/g" | sed "s/TEMPL2/$l2/g"| sed "s/T0/$t0/g" | sed "s/T1/$t1/g" | sed "s/RND/$rnd/g" | sed "s/TEMPE/$E/g" > /tmp/log_1.g
gappa /tmp/log_1.g >& /tmp/log_1.out
