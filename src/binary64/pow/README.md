### Makefile targets

- `check_exact` : Produces a file `ce.out`, to check all the exact and midpoint cases (with the exception of y in {1, 2, 3, 4}) run `./ce.out`.

- `exact` : Produces a file `e.out`
