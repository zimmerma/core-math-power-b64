# return the 'ulp' of the interval x, i.e., max(ulp(t)) for t in x
# this internal routine is used below
def RIFulp(x):
   return max(x.lower().ulp(),x.upper().ulp())

# bound the relative error of q_1()
# zh and zl are intervals
# zh=RIF(0.00013114734375,0.000132547)
# zl=RIF(-2^-42.6,2^-42.6)
# analyze_q1(zh,zl,verbose=true)
# (5.097868137842800?e-23, 1.000132?, 0.?e-12)
# zh=RR("-0x1.360e839e14d4dp-14",16)
# zl=RR("0x1.47286c85ae086p-42",16)
# analyze_q1(RIF(zh),RIF(zl),verbose=true)
# (4.673086383398838?e-23, 0.999926079455399?, 2.91?e-13)
# zh=RR(2^-968.9); zl=zh.ulp()
# analyze_q1(RIF(-zh,zh),RIF(-zl,zl),verbose=true)
# (4.54205817643525?e-23, 1.000000000000000?, 0.?e-15)
def analyze_q1(zh,zl,rel=false,verbose=false):
   err0 = 2^-74.221 # approximation error from the Sollya polynomial
   if verbose:
      print ("err0=", log(err0)/log(2.))
   z = zh + zl
   Q_1 = ["0x1p0","0x1p0","0x1p-1","0x1.55555559bc288p-3","0x1.555555586336ep-5"]
   Q_1 = [RR(x,16) for x in Q_1]
   # q = __builtin_fma (Q_1[4], zh, Q_1[3])
   q = Q_1[4]*zh+Q_1[3]
   # |q4| < 2^-4.58
   assert abs(Q_1[4]) < 2^-4.58, "abs(Q_1[4]) < 2^-4.58"
   # |zh| < 2^-12.88
   assert abs(zh).upper() < 2^-12.88, "abs(zh).upper() < 2^-12.88"
   # |q3| < 2^-2.58
   assert abs(Q_1[3]) < 2^-2.58, "abs(Q_1[3]) < 2^-2.58"
   err1a = RIFulp(q)
   err1b = abs(Q_1[4]*zl).upper()
   err1 = (err1a+err1b)*abs(z^3).upper()
   assert err1 < 2^-75.0448, "err1 < 2^-75.0448"
   if verbose:
      print ("err1=", log(err1)/log(2.))
   # q = __builtin_fma (q, z, Q_1[2])
   qin = q
   q = qin*z+Q_1[2]
   err2a = RIFulp(q)
   err2b = abs(z).upper().ulp()*abs(qin).upper()
   err2 = (err2a+err2b)*abs(z^2).upper()
   if verbose:
      print ("err2=", log(err2)/log(2.))
   # fast_two_sum (hi, lo, Q_1[1], q * z)
   qz = q*z
   err3a = abs(z).upper().ulp()*abs(q).upper()
   # err3a is the rounding error in z = zh + zl multiplied by q
   err3b = RIFulp(qz)
   hi = Q_1[1] + qz
   u = RIFulp(hi)
   lo = RIF(-u,u)
   err3c = 2^-105*abs(hi).upper()
   err3 = (err3a+err3b+err3c)*abs(z).upper()
   if verbose:
      print ("err3=", log(err3)/log(2.))
   # d_mul (hi, lo, zh, zl, *hi, *lo)
   #   a_mul (hi_out, s, zh, hi)
   hi_out = zh*hi
   u = RIFulp(hi_out)
   s = RIF(-u,u)
   #   t = fma (zl, hi, s)
   t = zl*hi+s
   #   lo_out = fma (zh, lo, t)
   lo_out = zh*lo+t
   err4a = abs(zl*lo).upper()
   err4b = RIFulp(t)
   err4c = RIFulp(lo_out)
   err4d = RIF(-2^-1074,2^-1074)
   err4 = err4a+err4b+err4c+err4d
   if verbose:
      print ("err4=", log(err4)/log(2.))
   hi,lo = hi_out,lo_out
   # fast_sum (hi, lo, Q_1[0], *hi, *lo)
   lo_in = lo
   hi = Q_1[0]+hi
   u = RIFulp(hi)
   lo = RIF(-u,u)
   lo += lo_in
   err5a = 2^-105*hi.abs().upper()
   err5b = RIFulp(lo)
   err5 = err5a+err5b
   if verbose:
      print ("err5=", log(err5)/log(2.))
   err = err0+err1+err2+err3+err4+err5
   # replace by err = err1, err = err2, ... to get the maximal value of
   # err1, err2, ...
   if rel:
      err = err/(hi+lo).abs().lower()
   if verbose:
      print ("err=", log(err)/log(2.))
   return err, hi, lo

# analyze_q1_all(k=8)
# zmin= 0.000131511476562500 zmax= 0.000132547000000000 err= -74.05445212551462?
# (5.097868137842800?e-23, 1.00013255578475, 1.50242725037943e-13)
# analyze_q1_all(k=8,rel=true)
# (5.09719775376001?e-23, 1.00013255578475, 1.50242725037943e-13)
def analyze_q1_all(k=0,rel=false):
   # split [-0.000132547,0.000132547] into 2^k intervals
   maxerr = maxhi = maxlo = 0
   h = 2*0.000132547/2^k
   for i in range(2^k):
      zmin = -0.000132547+i*h
      zmax = zmin+h
      zh = RIF(zmin,zmax)
      zl = RIF(-2^-42.6,2^-42.6)
      err, hi, lo = analyze_q1(zh,zl,rel=rel)
      if err>maxerr:
         print ("zmin=", zmin, "zmax=", zmax, "err=", log(err)/log(2.))
         maxerr = err
      if hi.abs().upper()>maxhi:
         # print ("zmin=", zmin, "zmax=", zmax, "hi=", hi.abs().upper())
         maxhi = hi.abs().upper()
      if lo.abs().upper()>maxlo:
         # print ("zmin=", zmin, "zmax=", zmax, "lo=", lo.abs().upper())
         maxlo = lo.abs().upper()
   return maxerr, maxhi, maxlo
     
def fast_two_sum(a,b):
   hi = a + b
   e = hi - a
   lo = b-e
   return hi,lo

def a_mul(a,b):
   hi = a * b
   lo = fma (a, b, -hi)
   return hi,lo

def d_mul(ah,al,bh,bl):
   hi, s = a_mul (ah, bh)
   t = fma (al, bh, s)
   lo = fma (ah, bl, t)
   return hi,lo

def fast_sum(a,bh,bl):
   hi, lo = fast_two_sum (a, bh)
   lo += bl
   return hi,lo

# zh=RR(2^-968.9); zl=zh.ulp()
# hi,lo = q_1(zh,zl)
def q_1(zh,zl,rnd='RNDN'):
   R = RealField(53,rnd=rnd)
   zh = R(zh)
   zl = R(zl)
   z = zh + zl
   q0 = R("0x1p0",16)
   q1 = R("0x1p0",16)
   q2 = R("0x1p-1",16)
   q3 = R("0x1.55555559bc288p-3",16)
   q4 = R("0x1.555555586336ep-5",16)
   q = fma(q4,zh,q3)
   q = fma(q,z,q2)
   hi,lo = fast_two_sum(q1,q*z)
   hi,lo = d_mul(zh,zl,hi,lo)
   hi,lo = fast_sum (q0, hi, lo)
   return hi, lo

# analyze_phpl()
# min N 0 0 0x1p+0 0x0p+0
# max U 63 63 0x1.ffe9d237fe371p+0 0x1.0f620ce383afdp-53
def analyze_phpl():
   minphpl = infinity
   maxphpl = -infinity
   for r in 'NZUD':
      R = RealField(53,rnd='RND'+r)
      for i1 in range(64):
         h1 = RR(n(2^(i1/2^12),200))
         l1 = RR(n(2^(i1/2^12)-h1.exact_rational(),200))
         h1 = R(h1)
         l1 = R(l1)
         for i2 in range(64):
            h2 = RR(n(2^(i2/64),200))
            l2 = RR(n(2^(i2/64)-h2.exact_rational(),200))
            h2 = R(h2)
            l2 = R(l2)
            ph, s = a_mul(h1,h2)
            t = fma(l1,h2,s)
            pl = fma(h1,l2,t)
            phpl = ph.exact_rational()+pl.exact_rational()
            if phpl<minphpl:
               minphpl = phpl
               print ("min",r,i1,i2,get_hex(ph),get_hex(pl))
            if phpl>maxphpl:
               maxphpl = phpl
               print ("max",r,i1,i2,get_hex(ph),get_hex(pl))

# analyze_hl()
# min N 0 0 0x1.ffeea0c164b0ep-1 -0x1.39152c171b528p-43
# max U 63 63 0x1.fffb314cd7a2ap+0 0x1.38f39de59f71dp-42
def analyze_hl():
   minhl = infinity
   maxhl = -infinity
   for r in 'NZUD':
      R = RealField(53,rnd='RND'+r)
      for i1 in range(64):
         h1 = RR(n(2^(i1/2^12),200))
         l1 = RR(n(2^(i1/2^12)-h1.exact_rational(),200))
         h1 = R(h1)
         l1 = R(l1)
         for i2 in range(64):
            h2 = RR(n(2^(i2/64),200))
            l2 = RR(n(2^(i2/64)-h2.exact_rational(),200))
            h2 = R(h2)
            l2 = R(l2)
            ph, s = a_mul(h1,h2)
            t = fma(l1,h2,s)
            pl = fma(h1,l2,t)
            # max value of qh+ql
            qh = R("0x1.0008afeac2a7p+0",16)
            ql = R("0x1.39152c171b528p-43",16) # largest number < 2^{-42.7096}
            h, s = a_mul(ph,qh)
            t = fma(pl,qh,s)
            l = fma(ph,ql,t)
            hl = h.exact_rational()+l.exact_rational()
            if hl<minhl:
               minhl = hl
               print ("min",r,i1,i2,get_hex(h),get_hex(l))
            if hl>maxhl:
               maxhl = hl
               print ("max",r,i1,i2,get_hex(h),get_hex(l))
            # min value of qh+ql
            qh = R("0x1.ffeea0c164b0ep-1",16)
            ql = R("-0x1.39152c171b528p-43",16)
            h, s = a_mul(ph,qh)
            t = fma(pl,qh,s)
            l = fma(ph,ql,t)
            hl = h.exact_rational()+l.exact_rational()
            if hl<minhl:
               minhl = hl
               print ("min",r,i1,i2,get_hex(h),get_hex(l))
            if hl>maxhl:
               maxhl = hl
               print ("max",r,i1,i2,get_hex(h),get_hex(l))

# rh=RR("-0x1.6232bb11e36d3p+9",16)
# rl=rh*2^-23.8899
# h,l=emulate_exp1(rh,rl)
def emulate_exp1(rh,rl,rnd='RNDN'):
   R = RealField(53,rnd=rnd)
   rh = R(rh)
   rl = R(rl)
   INVLN2 = R(6497320848556798/2^40)
   k = round(rh*INVLN2)
   LN2H = R("0x1.62e42fefa39efp-13",16)
   LN2L = R("0x1.abc9e3b39803fp-68",16)
   kh,kl = a_mul(R(k),LN2H)
   kl = fma(R(k),LN2L,kl)
   zh,zl = fast_sum(rh-kh,rl,-kl)
   j = k % 2^12
   e = (k-j)//2^12
   i2, i1 = divmod(j,64)
   assert k == e*2^12+i2*2^6+i1, "k == e*2^12+i2*2^6+i1"
   assert 0 <= i2 < 64, "0 <= i2 < 64"
   assert 0 <= i1 < 64, "0 <= i1 < 64"
   h1 = RR(n(2^(i1/2^12),200))
   l1 = RR(n(2^(i1/2^12)-h1.exact_rational(),200))
   h1 = R(h1)
   l1 = R(l1)
   h2 = RR(n(2^(i2/64),200))
   l2 = RR(n(2^(i2/64)-h2.exact_rational(),200))
   h2 = R(h2)
   l2 = R(l2)
   ph, s = a_mul(h1,h2)
   t = fma(l1,h2,s)
   pl = fma(h1,l2,t)
   qh,ql = q_1(zh,zl,rnd=rnd)
   h, s = a_mul(ph,qh)
   t = fma(pl,qh,s)
   l = fma(ph,ql,t)
   return 2^e*h,2^e*l

# get_hex(find_rho0())
# '-0x1.74910ee4e8a27p+9'
def find_rho0():
   R=RealField(200)
   a=RR("-0x1p+10",16)
   b=RR("-0x1p+9",16)
   elog=R(2^(-670544/10000))
   while true:
      c = (a+b)/2
      ylogx = R(c)*(1-R(2^(-2389/100)))*(1-R(2^-52))/(1+elog)
      if exp(ylogx).exact_rational()<2^-1075:
         a = c
      else:
         b = c
      if a.nextabove()==b:
         break
   return b

# get_hex(find_rho3())
# '0x1.62e4316ea5df9p+9'
def find_rho3():
   R=RealField(200)
   a=RR("0x1p+9",16)
   b=RR("0x1p+10",16)
   elog=R(2^(-670544/10000))
   while true:
      c = (a+b)/2
      ylogx = R(c)*(1-R(2^(-2389/100)))*(1-R(2^-52))/(1+elog)
      if exp(ylogx).exact_rational()<=2^1024*(1-2^-54):
         a = c
      else:
         b = c
      if a.nextabove()==b:
         break
   return a

# get_hex(find_rho1())
# '-0x1.577453f1799a6p+9'
# get_hex(find_rho1(h_alone=true))
# '-0x1.577453f1799a6p+9'
def find_rho1(h_alone=false):
   a = RR("-0x1p+10",16)
   b = RR("-0x1p+9",16)
   while true:
      c = (a+b)/2
      # from Lemma 5 we know |rl/rh| < 2^-23.8899
      rl = abs(c*2^-23.8899)
      C = c.exact_rational()
      while n(abs(rl.exact_rational()/C)/2^(-238899/10^4),200) <= 1:
         rl = rl.nextabove()
      while n(abs(rl.exact_rational()/C)/2^(-238899/10^4),200) > 1:
         rl = rl.nextbelow()
      rl = -rl # we want the smallest value possible
      h, l = emulate_exp1(c,rl,rnd='RNDD')
      if h_alone:
         d = h.exact_rational()
      else:
         d = h.exact_rational()+l.exact_rational()
      if d < 2^-991:
         a = c
      else:
         b = c
         print (get_hex(h), get_hex(l))
      if a.nextabove()==b:
         break
   return b # b is rho1

# get_hex(find_rho2())
# '0x1.62e42e709a95bp+9'
# get_hex(find_rho2(h_alone=true))
# '0x1.62e42e709a95bp+9'
def find_rho2(h_alone=false):
   a = RR("0x1p+9",16)
   b = RR("0x1p+10",16)
   Omega = RR(2^1024).nextbelow()
   while true:
      c = (a+b)/2
      # from Lemma 5 we know |rl/rh| < 2^-23.8899
      rl = abs(c*2^-23.8899)
      C = c.exact_rational()
      while n(abs(rl.exact_rational()/C)/2^(-238899/10^4),200) <= 1:
         rl = rl.nextabove()
      while n(abs(rl.exact_rational()/C)/2^(-238899/10^4),200) > 1:
         rl = rl.nextbelow()
      # we want the largest value possible, thus don't negate rl
      h, l = emulate_exp1(c,rl,rnd='RNDU')
      if h_alone:
         d = h.exact_rational()
      else:
         d = h.exact_rational()+l.exact_rational()
      if d <= Omega:
         a = c
         print (get_hex(h), get_hex(l))
      else:
         b = c
      if a.nextabove()==b:
         break
   return a # b is rho2
