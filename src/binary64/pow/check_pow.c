/* Check correctness of CORE-MATH's pow on exact and midpoint cases.

Copyright (c) 2022 Stéphane Glondu, Inria, Tom Hubrecht, ENS.

This file is part of the CORE-MATH project
(https://core-math.gitlabpages.inria.fr/).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/*
  To compile :
  gcc -fno-builtin -Wall -Wextra -lmpfr check_pow.c
*/

#define _POSIX_C_SOURCE 200809L /* for getline */

#include <fenv.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include <math.h>
#include <mpfr.h>

#include "pow.c"

static const mpfr_rnd_t rnd2[] = {MPFR_RNDN, MPFR_RNDZ, MPFR_RNDU, MPFR_RNDD};
static const int rnd1[] = {FE_TONEAREST, FE_TOWARDZERO, FE_UPWARD, FE_DOWNWARD};

static int rnd = 0;          /* default is to nearest */
static int count_errors = 0; /* default to fail on the first error */

/* reference code using MPFR */
double ref_pow(double x, double y) {
  mpfr_t z, _x, _y;
  mpfr_inits(z, _x, _y, NULL);
  mpfr_set_d(_x, x, MPFR_RNDN);
  mpfr_set_d(_y, y, MPFR_RNDN);
  int inex = mpfr_pow(z, _x, _y, rnd2[rnd]);
  mpfr_subnormalize(z, inex, rnd2[rnd]);
  double ret = mpfr_get_d(z, MPFR_RNDN);
  mpfr_clears(z, _x, _y, NULL);
  return ret;
}

void doloop(void) {
  char *buf = NULL;
  size_t buflength = 0;
  ssize_t n;
  double x, y, z1, z2;
  int count = 0;
  int errors = 0;

  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073);
  mpfr_set_emax(1024);

  while ((n = getline(&buf, &buflength, stdin)) >= 0) {
    if (n > 0 && buf[0] == '#')
      continue;
    if (sscanf(buf, "%la,%la", &x, &y) == 2) {
      z1 = ref_pow(x, y);
      fesetround(rnd1[rnd]);
      z2 = cr_pow(x, y);
      if (z1 != z2) {
        if (!count_errors) {
          printf("FAIL x := %la y := %la ref=%la z=%la\n", x, y, z1, z2);
          fflush(stdout);
          exit(1);
        }
        errors++;
      }
      count++;
    } else {
      printf(buf, NULL);
    }
  }
  free(buf);

  printf("%d tests passed\n", count);

  if (count_errors)
    printf("%d errors\n", errors);
}

int main(int argc, char *argv[]) {
  while (argc >= 2) {
    if (strcmp(argv[1], "--rndn") == 0) {
      rnd = 0;
      argc--;
      argv++;
    } else if (strcmp(argv[1], "--rndz") == 0) {
      rnd = 1;
      argc--;
      argv++;
    } else if (strcmp(argv[1], "--rndu") == 0) {
      rnd = 2;
      argc--;
      argv++;
    } else if (strcmp(argv[1], "--rndd") == 0) {
      rnd = 3;
      argc--;
      argv++;
    } else if (strcmp(argv[1], "--count") == 0) {
      count_errors = 1;
      argc--;
      argv++;
    } else {
      fprintf(stderr, "Error, unknown option %s\n", argv[1]);
      exit(1);
    }
  }

  doloop();
}
