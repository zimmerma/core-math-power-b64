/* Generate special cases for pow testing.

Copyright (c) 2022 Stéphane Glondu, Paul Zimmermann, Inria.

This file is part of the CORE-MATH project
(https://core-math.gitlabpages.inria.fr/).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <fenv.h>
#include <math.h>
#include <float.h>
#include <mpfr.h>
#include <omp.h>

int ref_fesetround (int);
void ref_init (void);

double ref_pow (double, double);
double cr_pow  (double, double);

int rnd1[] = { FE_TONEAREST, FE_TOWARDZERO, FE_UPWARD, FE_DOWNWARD };

int rnd = 0;
int verbose = 0;

static void
check (double x, double y)
{
  double z1, z2;
  z1 = ref_pow (x, y);
  fesetround(rnd1[rnd]);
  z2 = cr_pow (x, y);
  if (z1 != z2)
  {
    printf ("FAIL x,y=%la,%la ref=%la z=%la\n", x, y, z1, z2);
    fflush (stdout);
    exit (1);
  }
}

/* round x^y toward zero */
static double
pow_rz (double x, double y)
{
  mpfr_t xx, yy;
  /* emin is set to -1073 by ref_init */
  mpfr_init2 (xx, 53);
  mpfr_init2 (yy, 53);
  mpfr_set_d (xx, x, MPFR_RNDN); /* exact */
  mpfr_set_d (yy, y, MPFR_RNDN); /* exact */
  int ret = mpfr_pow (yy, xx, yy, MPFR_RNDZ);
  mpfr_subnormalize (yy, ret, MPFR_RNDZ);
  double z = mpfr_get_d (yy, MPFR_RNDN); /* exact */
  mpfr_clear (xx);
  mpfr_clear (yy);
  return z;
}

/* round x^y toward +Inf */
static double
pow_ru (double x, double y)
{
  mpfr_t xx, yy;
  /* emin is set to -1073 by ref_init */
  mpfr_init2 (xx, 53);
  mpfr_init2 (yy, 53);
  mpfr_set_d (xx, x, MPFR_RNDN); /* exact */
  mpfr_set_d (yy, y, MPFR_RNDN); /* exact */
  int ret = mpfr_pow (yy, xx, yy, MPFR_RNDU);
  mpfr_subnormalize (yy, ret, MPFR_RNDU);
  double z = mpfr_get_d (yy, MPFR_RNDN); /* exact */
  mpfr_clear (xx);
  mpfr_clear (yy);
  return z;
}

/* check that x^y is exact or midpoint */
static int
is_exact_or_midpoint (double x, double y)
{
  mpfr_t xx, yy, zz;
  /* here we need to set emin to -1074 to check midpoints */
  int emin = mpfr_get_emin ();
  mpfr_init2 (xx, 53);
  mpfr_init2 (yy, 53);
  mpfr_init2 (zz, 54); /* use 54 bits for midpoints */
  mpfr_set_d (xx, x, MPFR_RNDN); /* exact */
  mpfr_set_d (yy, y, MPFR_RNDN); /* exact */
  int ret = mpfr_pow (zz, xx, yy, MPFR_RNDZ);
  mpfr_set_emin (-1074);
  ret = mpfr_subnormalize (zz, ret, MPFR_RNDZ);
  mpfr_clear (xx);
  mpfr_clear (yy);
  mpfr_clear (zz);
  mpfr_set_emin (emin);
  return ret == 0;
}

/* Check exact/midpoint cases in the subnormal range: 2^-1075 <= x^y < 2^-1022,
   for y integer, 2 <= y <= 35. */
static void
check_exact_subnormal1 (double y)
{
  if (verbose)
#pragma omp critical
    printf ("Checking exact/midpoint subnormal output for y=%la\n", y);
  double xmin, xmax, x;
  xmin = pow_rz (0x1p-1074, 1.0 / y);
  while (pow_rz (xmin, y) == 0.0)
    xmin = nextafter (xmin, DBL_MAX);
  while (pow_rz (nextafter (xmin, 0.0), y) > 0.0)
    xmin = nextafter (xmin, 0.0);
  /* now xmin is the smallest number such that RN(xmin^y) > 0 */
  xmax = pow_rz (0x1p-1022, 1.0 / y);
  while (pow_rz (xmax, y) < 0x1p-1022)
    xmax = nextafter (xmax, DBL_MAX);
  while (pow_rz (nextafter (xmax, 0.0), y) >= 0x1p-1022)
    xmax = nextafter (xmax, 0.0);
  /* Assume x = m*2^e with m odd: if m >= 2^k, then m^y >= 2^(yk),
     thus x^y needs at least yk+1 bits to be representable exactly,
     thus we need yk+1 <= 54, i.e., y <= floor(53/k).
     We use 54 here since we also want exact midpoints. */
  int p = 1 + (53 / (int) y); /* maximal number of bits of m */
  mpfr_t xx;
  mpfr_init2 (xx, p);
  mpfr_set_d (xx, xmin, MPFR_RNDU);
  while (mpfr_cmp_d (xx, xmax) < 0)
  {
    x = mpfr_get_d (xx, MPFR_RNDN); /* exact */
    if (is_exact_or_midpoint (x, y))
    {
      check (x, y);
      check (-x, y);
    }
    mpfr_nextabove (xx);
  }
  mpfr_clear (xx);
  if (verbose)
#pragma omp critical
    printf ("   done y=%la\n", y);
}

/* return non-zero iff x is an e-th power */
int
is_eth_power (mpfr_t x, int e)
{
  mpfr_t y;
  mpfr_init2 (y, 53);
  int ret = mpfr_rootn_ui (y, x, e, MPFR_RNDN);
  mpfr_clear (y);
  return ret == 0;
}

/* for y=2^F*n */
static void
check_exact_subnormal2 (int F, int n)
{
  double y = ldexp ((double) n, F), xmin, xmax, x;
  if (verbose)
#pragma omp critical
    printf ("Checking exact/midpoint subnormal output for y=%la\n", y);
  xmin = pow_rz (0x1p-1074, y);
  if (xmin >= 0x1p-1022) /* x^y is never in the subnormal range */
    return;
  xmin = pow_rz (0x1p-1074, 1.0 / y);
  while (pow_rz (xmin, y) == 0.0)
    xmin = nextafter (xmin, DBL_MAX);
  while (pow_rz (nextafter (xmin, 0.0), y) > 0.0)
    xmin = nextafter (xmin, 0.0);
  /* now xmin is the smallest number such that RN(xmin^y) > 0 */
  xmax = pow_rz (0x1p-1022, 1.0 / y);
  while (pow_rz (xmax, y) < 0x1p-1022)
    xmax = nextafter (xmax, DBL_MAX);
  while (pow_rz (nextafter (xmax, 0.0), y) >= 0x1p-1022)
    xmax = nextafter (xmax, 0.0);
  int e = pow (2.0, - (double) F); /* e = 2, 4, 8, 16 or 32 */
  /* Assume x = t^e with t=m*2^f and m odd: if m >= 2^k, then m^n >= 2^(nk),
     thus x^y needs at least nk+1 bits to be representable exactly,
     thus we need nk+1 <= 54, i.e., n <= floor(53/k).
     We use 54 here since we also want exact midpoints. */
  int p = 1 + (53 / n); /* maximal number of bits of m */
  /* we want x to be an e-th power: xmin <= x = t^e < xmax
     thus xmin^(1/e) <= t < xmax^(1/e) */
  double t, tmin, tmax;
  tmin = pow_rz (xmin, 1.0 / (double) e);
  tmax = pow_rz (xmax, 1.0 / (double) e);
  mpfr_t tt;
  mpfr_init2 (tt, p);
  mpfr_set_d (tt, tmin, MPFR_RNDU);
  while (mpfr_cmp_d (tt, tmax) < 0)
  {
    t = mpfr_get_d (tt, MPFR_RNDN); /* exact */
    x = pow (t, (double) e);
    if (is_exact_or_midpoint (x, y))
      check (x, y);
    mpfr_nextabove (tt);
  }
  mpfr_clear (tt);
  if (verbose)
#pragma omp critical
    printf ("   done y=%la\n", y);
}

/* assume x < 1 */
static void
check_near_one_aux2 (double x)
{
  /* first compute the largest y such that x^y does not underflow */
  double y0, y1;
  y0 = 1.0;
  while (pow_rz (x, 2 * y0) != 0)
    y0 = 2.0 * y0;
  /* now x^y0 does not underflow, but x^(2y0) does */
  y1 = 2.0 * y0;
  while (1)
  {
    double m = (y0 + y1) * 0.5;
    if (m == y0 || m == y1)
      break;
    if (pow_rz (x, m) == 0) /* x^m underflows */
      y1 = m;
    else
      y0 = m;
  }
  check (x, y0);
}

/* assume 1 <= x */
static void
check_near_one_aux3 (double x)
{
  if (x == 1.0) /* the code below assumes x > 1 */
    return;
  /* first compute the largest y such that x^y does not overflow */
  double y0, y1;
  y0 = 1.0;
  while (pow_ru (x, 2 * y0) <= DBL_MAX)
    y0 = 2.0 * y0;
  /* now x^y0 does not overflow, but x^(2y0) does */
  y1 = 2.0 * y0;
  while (1)
  {
    double m = (y0 + y1) * 0.5;
    if (m == y0 || m == y1)
      break;
    if (pow_ru (x, m) == 0) /* x^m overflows */
      y1 = m;
    else
      y0 = m;
  }
  check (x, y0);
  /* check also y0/2 */
  check (x, y0 / 2.0);
}

/* check 1 - (2i+1)/2^53, 1 - (2i+2)/2^53, and 1+i/2^52 */
static void
check_near_one_aux (uint64_t i)
{
  double x;
  x = (double) (2 * i + 1) * 0x1p-53;
  check_near_one_aux2 (1.0 - x);
  x = (double) (2 * i + 2) * 0x1p-53;
  check_near_one_aux2 (1.0 - x);
  x = (double) i * 0x1p-52;
  check_near_one_aux3 (1.0 + x);
}

/* check the code for 255/256 <= x < 257/256 */
static void
check_near_one (void)
{
  /* there are 2^45 values in [255/256,1), and 2^44 in [1,257/256) */
#pragma omp parallel for
  // for (uint64_t i = 0; i < 0x100000000000; i++)
  for (uint64_t i = 0x100000000000; i < 0x200000000000; i++)
    check_near_one_aux (i);
}

/* Test from "The Mathematical Function Computation Handbook" by Nelson Beebe,
   Section 14.14:
   compare (x*x)^(3/2) with x^3 and random x in [1/beta,1], purified so that
   the product x*x is exactly representable. We use the "purification"
   procedure proposed by Beebe: if t is the working precision (here t=53),
   compute sigma = x*beta^floor((t+1)/2)), here sigma = x*2^27, then
   replace x by (x + sigma) - sigma. */
static void
check_beebe2 (unsigned long n)
{
  for (unsigned long i = 0; i < n; i++)
  {
    double x = 0.5 + drand48 () * 0.5;
    double sigma = x * 0x1p27;
    x = (x + sigma) - sigma;
    double z1 = cr_pow (x * x, 1.5);
    double z2 = cr_pow (x, 3.0);
    if (z1 != z2)
    {
      printf ("Test Beebe2 failed for x=%la\n", x);
      printf ("Got z1=%la\n", z1);
      printf ("Got z2=%la\n", z2);
      exit (1);
    }
  }
  printf ("Test Beebe2 succeeded for %lu random values\n", n);
}

/* same as check_beebe2, but with random x in [1,(maximum normal)^(1/3)] */
static void
check_beebe3 (unsigned long n)
{
  for (unsigned long i = 0; i < n; i++)
  {
    double x = 1.0 + drand48 (); /* 1 <= x <= 2 */
    x = ldexp (x, lrand48 () % 341);
    double sigma = x * 0x1p27;
    x = (x + sigma) - sigma;
    double z1 = cr_pow (x * x, 1.5);
    double z2 = cr_pow (x, 3.0);
    if (z1 != z2)
    {
      printf ("Test Beebe3 failed for x=%la\n", x);
      printf ("Got z1=%la\n", z1);
      printf ("Got z2=%la\n", z2);
      exit (1);
    }
  }
  printf ("Test Beebe3 succeeded for %lu random values\n", n);
}

/* compare (x*x)^(y/2) with x^y, with random x on [1/100,10], and random y on
   [-Y,+Y], where Y is the largest number that avoids both underflow and
   overflow in x^Y. Purify x as in the first test (check_beebe2) so that x^2
   is exact. Purify y so that y/2 is exact: no need since we are in radix 2. */
static void
check_beebe4 (unsigned long n)
{
#define Y 161.653 // log(2^-1074.)/log(1/100.)
  for (unsigned long i = 0; i < n; i++)
  {
    double x = 0.01 + 9.99 * drand48 (); /* 1/100 <= x <= 10 */
    double sigma = x * 0x1p27;
    x = (x + sigma) - sigma;
    double y = Y * (2.0 * drand48 () - 1.0);
    double z1 = cr_pow (x * x, y / 2.0);
    double z2 = cr_pow (x, y);
    if (z1 != z2)
    {
      printf ("Test Beebe4 failed for x=%la, y=%la\n", x, y);
      printf ("Got z1=%la\n", z1);
      printf ("Got z2=%la\n", z2);
      exit (1);
    }
  }
  printf ("Test Beebe4 succeeded for %lu random values\n", n);
}

int
main (int argc, char *argv[])
{
  while (argc >= 2)
    {
      if (strcmp (argv[1], "--rndn") == 0)
        {
          rnd = 0;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "--rndz") == 0)
        {
          rnd = 1;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "--rndu") == 0)
        {
          rnd = 2;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "--rndd") == 0)
        {
          rnd = 3;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "--verbose") == 0)
        {
          verbose = 1;
          argc --;
          argv ++;
        }
      else
        {
          fprintf (stderr, "Error, unknown option %s\n", argv[1]);
          exit (1);
        }
    }

  ref_init ();
  ref_fesetround (rnd);

#define BEEBE_NTESTS 1000000000
  check_beebe2 (BEEBE_NTESTS);
  check_beebe3 (BEEBE_NTESTS);
  check_beebe4 (BEEBE_NTESTS);

  check_near_one ();

  /* Warning: the following test takes a very long time, even on a
     multicore machine. */
  /* We use for y the set S from
     https://hal-ens-lyon.archives-ouvertes.fr/ensl-00169409 (page 9) */
#pragma omp parallel for schedule(dynamic,1)
  for (int y = 2; y <= 35; y++)
    check_exact_subnormal1 ((double) y);
  /* loop over -5 <= F <= -1 and 3 <= n <= 35 with n odd */
#pragma omp parallel for schedule(dynamic,1)
  for (int i = 0; i < 85; i++)
  {
    int F = i / 17 - 5;
    int n = 3 + 2 * (i % 17);
    check_exact_subnormal2 (F, n);
  }

  return 0;
}
