# change the rounding mode here to ne (nearest), zr (toward zero),
# up (upward) and dn (downward) to get a bound for all rounding modes
@rnd = float<ieee_64,zr>;

p2 = -0x1p-1;
p3 = 0x1.5555555555558p-2;
p4 = -0x1.0000000000003p-2;
p5 = 0x1.999999981f535p-3;
p6 = -0x1.55555553d1eb4p-3;
p7 = 0x1.2494526fd4a06p-3;
p8 = -0x1.0001f0c80e8cep-3;

# variables starting with uppercase letters represent exact values
# (no rounding error)
Zz = z * z;

# a_mul (&wh, &wl, z, z)
wh rnd= z * z;
# wl rnd= fma (z, z, -wh);
# we have to help Gappa here to tell that wl is exact
# but we add rnd(wl) = wl in the conclusion to check it
wl = z*z-wh;

# t = __builtin_fma (P_1[5], z, P_1[4])
t rnd= fma (p8, z, p7);
T = p8*z+p7;

# u = __builtin_fma (P_1[3], z, P_1[2])
u rnd= fma (p6, z, p5);
U = p6*z+p5;

# v = __builtin_fma (P_1[1], z, P_1[0])
v rnd= fma (p4, z, p3);
V = p4*z+p3;

# u = __builtin_fma (t, wh, u)
u2 rnd= fma (t, wh, u);
U2 = t*wh+u;

# v = __builtin_fma (u, wh, v)
v2 rnd= fma (u2, wh, v);
V2 = u2*wh+v;

# u = v * wh
u3 rnd= v2 * wh;
U3 = v2 * wh;

# *hi = -0.5 * wh
hi rnd= -0.5 * wh;
Hi = -0.5 * wh;

# *lo = __builtin_fma (u, z, -0.5 * wl)
wl2 rnd= -0.5 * wl;
Wl2 = -0.5 * wl;
lo rnd= fma (u3, z, wl2);
Lo = u3*z+wl2;

err1 = (t - T)*Zz*Zz*Zz*z;      # error from the first fma
err2 = (u - U)*Zz*Zz*z;         # error from the 2nd fma
err3 = (v - V)*Zz*z;            # error from the 3rd fma
err4a = u2 - U2;                # rounding error from the 4th fma
err4b = t * wl;                 # neglected term t*wl in 4th fma
err4 = (err4a + err4b)*Zz*Zz*z; # total error in 4th fma
err5a = v2 - V2;                # rounding error from the 5th fma
err5b = u2 * wl;                # neglected term u*wl in 5th fma
err5 = (err5a + err5b)*Zz*z;    # total error in 5th fma
err6a = (u3-U3)*z;              # error from u = u * wh
err6b = -v2*wl*z;               # neglected u * wl term in u
err6 = err6a + err6b;
err7 = lo-Lo;                   # error from the 5th fma

{ @FLT(z,53) ->
  |z| <= 0.0040283203125 ->
  wh + wl = Zz -> # wh + wl = z*z exactly
  rnd(wl) = wl -> # wl is exactly representable
  hi = Hi ->      # hi is exact
  wl2 = Wl2 ->    # -0.5*wl is exact
  |err0| <= 2.68e-25 -> # mathematical error from the Sollya polynomial
# uncomment exactly one of the following lines to get the cumulated error,
# or the range for hi or lo respectively
  |err0+err1+err2+err3+err4+err5+err6+err7| in ?
#  |hi| in ?
#  |lo| in ?
} 

# Bounds for the different errors (with rnd=zr):
# err0:2^-81.626
# err1:2^-110.689
# err2:2^-94.7781
# err3:2^-77.8668
# err4:2^-94.7781
# err5:2^-77.8668
# err6:2^-77.9556
# err7:2^-78
