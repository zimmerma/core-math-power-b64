RQ = RealField(256, rnd="RNDD")


def print_qint(x):
    s, m, e = x.sign_mantissa_exponent()

    ll = m % 2 ^ 64
    m = m // 2 ^ 64

    lh = m % 2 ^ 64
    m = m // 2 ^ 64

    hl = m % 2 ^ 64
    hh = m // 2 ^ 64

    s = (1 - s) / 2

    print(
        f"    {{.hh=0x{hh:x}, .hl=0x{hl:x}, .lh=0x{lh:x}, .ll=0x{ll:x}, .ex={e + 255}, .sgn=0x{s}}},"
    )


def print_array(a, name):
    print(f"static const qint64_t {name}[] = {{")
    for x in a:
        print_qint(x)
    print("};\n")


def inverse(y):
    """
    Returns 1/y
    """
    Y = y.exact_rational()
    r = RQ((1 / Y).n(prec=64))

    return r


def _log(r):
    """Return -log(r)"""

    R = r.exact_rational()
    L = RQ(-R.log())

    return L


_INVERSE = []
_LOG_INV = []
_P_3 = []
_Q_3 = []
_T1 = []
_T2 = []


def compute(s):
    y = RR(s, 16)
    r = inverse(y.n(prec=12))
    l = _log(r)

    _INVERSE.append(r)
    _LOG_INV.append(l)


for h1 in "0123456789abcdef":
    for h2 in "048c":
        compute(f"0x1.{h1}{h2}p-1")

for h1 in "0123456":
    for h2 in "02468ace":
        compute(f"0x1.{h1}{h2}p+0")

for i in range(2 ^ 6):
    t1 = 2 ^ (i / 2 ^ 6)
    t2 = 2 ^ (i / 2 ^ 12)

    _T1.append(RQ(t1))
    _T2.append(RQ(t2))


for k in range(40):
    _P_3 = [RQ(((-1) ^ k) / (k + 1))] + _P_3

for k in range(30):
    _Q_3 = [RQ(1 / factorial(k))] + _Q_3

print_array(_INVERSE, "_INVERSE_3")
print_array(_LOG_INV, "_LOG_INV_3")
print_array(_T1, "T1_3")
print_array(_T2, "T2_3")
print_array(_P_3, "P_3")
print_array(_Q_3, "Q_3")
