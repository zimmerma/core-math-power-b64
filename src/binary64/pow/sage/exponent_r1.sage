def strf(x):
    (f, e) = float(x).hex().split("p")
    return f"{f.rstrip('0')}p{e}"


def print_array(a, name, d=1):
    print(f"static const double {name}[][2] = {{")
    for e in a:
        hi, lo = e
        print(f"    {{{strf(hi)}, {strf(lo)}}},")
    print("};\n")


def double(x):
    return (x.n(), x.n(prec=106) - x.n().exact_rational())


T1 = []
T2 = []

for i in range(2 ^ 6):
    t1 = 2 ^ (i / 2 ^ 6)
    t2 = 2 ^ (i / 2 ^ 12)

    T1.append(double(t1))
    T2.append(double(t2))

print_array(T1, "T1")
print_array(T2, "T2")
