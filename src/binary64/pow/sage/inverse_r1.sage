def strf(x):
    (f, e) = float(x).hex().split("p")
    return f"{f.rstrip('0')}p{e}"


def print_array(a, name, d=1):
    c = "[][2]" if d else "[]"
    print(f"static const double {name}{c} = {{")
    for e in a:
        if type(e) is tuple:
            hi, lo = e
            print(f"    {{{strf(hi)}, {strf(lo)}}},")
        else:
            print(f"    {strf(e)},")
    print("};\n")


def inverse(y):
    """
    Returns 1/y with 9 bits of precision rounded towards +inf
    """
    Y = y.exact_rational()
    r = (1 / Y + 2 ** (-9)).n(prec=9)

    return r


def _log(r):
    """
    Returns two doubles l1 and l2 such that l1 + l2 = -log(r)
    with at least 105 exact bits
    """

    R = r.exact_rational()
    L = -R.log().n(prec=106)
    l1 = L.n()
    l2 = (L - l1.exact_rational()).n()

    return (l1, l2)


_INVERSE = []
_LOG_INV = []


def compute(s):
    y = RR(s, 16)
    r = inverse(y)
    l = _log(r)

    _INVERSE.append(r)
    _LOG_INV.append(l)


for h1 in "0123456789abcdef":
    for h2 in "048c":
        compute(f"0x1.{h1}{h2}p-1")

for h1 in "0123456":
    for h2 in "02468ace":
        compute(f"0x1.{h1}{h2}p+0")

print_array(_INVERSE, "_INVERSE")
print_array(_LOG_INV, "_LOG_INV", 2)
