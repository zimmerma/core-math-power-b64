RD = RealField(128, rnd="RNDD")


def print_dint(x):
    s, m, e = x.sign_mantissa_exponent()
    hi = m // (2 ^ 64)
    lo = m % (2 ^ 64)
    s = (1 - s) / 2
    print(f"    {{.hi=0x{hi:x}, .lo=0x{lo:x}, .ex={e + 127}, .sgn=0x{s}}},")


def print_array(a, name):
    print(f"static const dint64_t {name}[] = {{")
    for x in a:
        print_dint(x)
    print("};\n")


T1 = []
T2 = []

for i in range(2 ^ 6):
    t1 = 2 ^ (i / 2 ^ 6)
    t2 = 2 ^ (i / 2 ^ 12)

    T1.append(RD(t1))
    T2.append(RD(t2))

print_array(T1, "T1_2")
print_array(T2, "T2_2")
