RD = RealField(128, rnd="RNDD")


def print_dint(x):
    s, m, e = x.sign_mantissa_exponent()
    hi = m // (2 ^ 64)
    lo = m % (2 ^ 64)
    s = (1 - s) / 2
    print(f"    {{.hi=0x{hi:x}, .lo=0x{lo:x}, .ex={e + 127}, .sgn=0x{s}}},")


def print_array(a, name):
    print(f"static const dint64_t {name}[] = {{")
    for x in a:
        print_dint(x)
    print("};\n")


def inverse(y):
    """
    Returns 1/y
    """
    Y = y.exact_rational()
    r = RD((1 / Y).n(prec=64))

    return r


def _log(r):
    """Return -log(r)"""

    R = r.exact_rational()
    L = RD(-R.log())

    return L


_INVERSE = []
_LOG_INV = []


def compute(s):
    y = RR(s, 16)
    r = inverse(y.n(prec=10))
    l = _log(r)

    _INVERSE.append(r)
    _LOG_INV.append(l)


for h1 in "0123456789abcdef":
    for h2 in "02468ace":
        compute(f"0x1.{h1}{h2}p-1")

for h1 in "0123456":
    for h2 in "0123456789abcdef":
        compute(f"0x1.{h1}{h2}p+0")

print_array(_INVERSE, "_INVERSE_2")
print_array(_LOG_INV, "_LOG_INV_2")
