#include "fenv_mpfr.h"
#include "pow.c"

#include <stdlib.h>
#include <string.h>
#include <time.h>

double cr_log(double x) {
  double hi, lo;
  log_1(x, &hi, &lo);

  return hi + lo;
};

/* reference code using MPFR */
double ref_log(double x) {
  mpfr_t z, _x;
  mpfr_inits2(53, z, _x, NULL);
  mpfr_set_d(_x, x, MPFR_RNDN);
  int inex = mpfr_log(z, _x, rnd2[rnd]);
  mpfr_subnormalize(z, inex, rnd2[rnd]);
  double ret = mpfr_get_d(z, MPFR_RNDN);
  mpfr_clears(z, _x, NULL);
  return ret;
}

int main(int argc, char *argv[]) {
  srand(time(NULL));

  double range = 0x1.0p+1023;
  double div = RAND_MAX / range;

  long errors = 0;
  long valid = 0;

  long n = 0x989680;

  if (argc >= 3 && strcmp(argv[1], "-n") == 0) {
    n = strtol(argv[2], NULL, 0);
  }

  printf("Errors :\n");

  for (long i = 1; i < n; i++) {
    double x = rand() / div;

    double z = cr_log(x);
    double z_ref = ref_log(x);

    if (__builtin_isfinite(z_ref)) {
      valid++;
      if (z != z_ref)
        errors++;
    }

    if (!(i % (n / 20))) {
      printf("  %ld / %ld - %la\n", errors, valid, (double)errors / valid);
    }
  }

  return 0;
}
