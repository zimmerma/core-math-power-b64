#define POW_ITERATION 15

#include "pow.c"
#include "pow_mpfr.c"

#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <fenv.h>

static const char *modes[] = {"to nearest", "towards zero", "towards +inf",
                              "towards -inf"};
static const int rnd1[] = {FE_TONEAREST, FE_TOWARDZERO, FE_UPWARD, FE_DOWNWARD};

int main(int argc, char *argv[]) {
  srand(time(NULL));

  ref_init();

  double range = 0x1.0p+8;
  double div = RAND_MAX / range;

  long n = 1000000000;

  if (argc >= 3 && strcmp(argv[1], "-n") == 0) {
    n = strtol(argv[2], NULL, 0);
  }

  for (rnd = 0; rnd < 4; rnd++) {
    long errors = 0;
    long valid = 0;

    fesetround(rnd1[rnd]);

    printf("\nErrors for round %s:\n", modes[rnd]);

    for (long i = 1; i < n + 1; i++) {
      double x = rand() / (double)(RAND_MAX / 0x1.0p+9);
      double y = rand() / div - (range / 2);

      double z = cr_pow(x, y);
      double z_ref = ref_pow(x, y);

      valid++;
      if (z != z_ref) {
        printf("\r    incorrect : %la %la\n", x, y);
        printf("\r  %ld / %ld - %la ", errors, valid, (double)errors / valid);
        fflush(stdout);

        errors++;
      }

      if (!(i % (n / 200))) {
        printf("\r  %ld / %ld - %la ", errors, valid, (double)errors / valid);
        fflush(stdout);
      }
    }
    printf("\n");
  }

  return 0;
}
