/* Check non-trivial exact and midpoint cases for binary64 power function.

Copyright (c) 2022 Paul Zimmermann, Inria, Tom Hubrecht, CERN.

This file is part of the CORE-MATH project
(https://core-math.gitlabpages.inria.fr/).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/* Exact cases are such that x^y is exactly representable in binary64.
   Midpoint cases are such that x^y is exactly in the middle of two
   representable binary64 numbers: the smallest possible value of x^y is 2^-1075
   (middle of 0 and 2^-1074), and the maximal one is 2^1024*(1-2^-53) (middle of
   DBL_MAX and 2^1024, which is not representable, but this would be a
   hard-to-round case for rounding to nearest). We don't count/print trivial
   solutions: |x|=1, y=0, y=1, y=0.5.

   $ gcc -W -Wall -Wextra -ftree-parallelize-loops=X exact.c -lm -lmpfr
   $ ./a.out # count all exact and midpoint cases
   ...
   # count=1071899
   $ ./a.out -mid 0 # count only exact cases
   ...
   # count=842073
*/

#include <assert.h>
#include <fenv.h>
#include <math.h>
#include <mpfr.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <omp.h>

#include "pow.h"

int print = 0;     /* by default, we only count exact and midpoint cases */
int mid = 1;       /* by default, we count or print midpoint cases */
int check = 0;     /* check solutions */
int subnormal = 1; /* allow subnormal numbers */

uint64_t count = 0;  /* number of solutions */
uint64_t errors = 0; /* number of errors */

int e_max = 1023;
int e_min = 1074;

static const char *modes[] = {"to nearest ..", "towards zero ", "towards +inf ",
                              "towards -inf "};
static const mpfr_rnd_t rnd2[] = {MPFR_RNDN, MPFR_RNDZ, MPFR_RNDU, MPFR_RNDD};
static const int rnd1[] = {FE_TONEAREST, FE_TOWARDZERO, FE_UPWARD, FE_DOWNWARD};

static int rnd; /* default is to nearest */
// static int count_errors = 0; /* default to fail on the first error */

double ref_pow(double x, double y) {
  mpfr_set_default_prec(53);
  mpfr_set_emin(-1073);
  mpfr_set_emax(1024);

  mpfr_t z, _x, _y;
  mpfr_inits(z, _x, _y, NULL);
  mpfr_set_d(_x, x, MPFR_RNDN);
  mpfr_set_d(_y, y, MPFR_RNDN);
  int inex = mpfr_pow(z, _x, _y, rnd2[rnd]);
  mpfr_subnormalize(z, inex, rnd2[rnd]);
  double ret = mpfr_get_d(z, MPFR_RNDN);
  mpfr_clears(z, _x, _y, NULL);
  return ret;
}

void check_sol(double x, double y) {
  double z1, z2;

  z1 = ref_pow(x, y);
  fesetround(rnd1[rnd]);
  z2 = cr_pow(x, y);

  if (z1 != z2) {
    printf("\nFAIL x := %la y := %la ref=%la z=%la\n", x, y, z1, z2);
    fflush(stdout);
    exit(1);
  }
}

/* y positive integer: both x and -x are solutions */
void check_uint_y(int32_t y) {
  double xmin, xmax;
  int emin, emax;
  mpfr_t z;
  unsigned long local_count = 0;
  mpfr_init2(z, 54);
  mpfr_set_ui_2exp(z, 1, -(e_min + 1), MPFR_RNDN);
  mpfr_rootn_ui(z, z, y, MPFR_RNDU);
  xmin = mpfr_get_d(z, MPFR_RNDU);
  mpfr_set_ui_2exp(z, 1, (e_max + 1), MPFR_RNDN);
  mpfr_nextbelow(z);
  mpfr_rootn_ui(z, z, y, MPFR_RNDD);
  xmax = mpfr_get_d(z, MPFR_RNDD);
  /* we want xmin <= x <= xmax, with x^y exact */
  /* write x = m*2^e with m odd, m >= 3 */
  /* compute the maximum odd m such that m^y < 2^(53+mid) */
  mpfr_set_ui_2exp(z, 1, 53 + mid, MPFR_RNDN);
  mpfr_nextbelow(z);
  mpfr_rootn_ui(z, z, y, MPFR_RNDD);
  assert(mpfr_fits_slong_p(z, MPFR_RNDD));
  int maxm = mpfr_get_ui(z, MPFR_RNDD);
  /* since m*2^e <= xmax, we have 2^e <= xmax/m <= xmax/3 */
  mpfr_set_d(z, xmax, MPFR_RNDD);
  mpfr_div_ui(z, z, 3, MPFR_RNDD);
  emax = mpfr_get_exp(z) - 1;
  /* for the minimal exponent, since x is an odd multiple of 2^e,
     x^y is an odd multiple of 2^(y*e), thus we want y*e >= -149-mid */
  emin = -((e_min + mid) / y);

  for (int e = emin; e <= emax; e++)
    for (int m = 3; m <= maxm; m += 2) {
      mpfr_set_ui_2exp(z, m, e, MPFR_RNDN);
      if (mpfr_cmp_d(z, xmin) >= 0 && mpfr_cmp_d(z, xmax) <= 0) {
        double x = mpfr_get_d(z, MPFR_RNDN);

        check_sol(x, (double)y);

        local_count++;

        check_sol(-x, (double)y);

        local_count++;
      }
    }
  mpfr_clear(z);
}

/* y = n/2^f */
void check_uint_2exp_y(int32_t f, int32_t n) {
  long F = 1 << f;
  double xmin, xmax;
  long emin, emax;
  mpfr_t z;
  unsigned long local_count = 0;
  double y = ldexp(n, -f);
  mpfr_init2(z, 54);
  mpfr_set_ui_2exp(z, 1, -(e_min + 1), MPFR_RNDN);
  mpfr_rootn_ui(z, z, n, MPFR_RNDU);
  mpfr_pow_ui(z, z, F, MPFR_RNDU);
  xmin = mpfr_get_d(z, MPFR_RNDU);
  mpfr_set_ui_2exp(z, 1, (e_max + 1), MPFR_RNDN);
  mpfr_nextbelow(z);
  mpfr_rootn_ui(z, z, n, MPFR_RNDD);
  mpfr_pow_ui(z, z, F, MPFR_RNDD);
  xmax = mpfr_get_d(z, MPFR_RNDD);
  /* we want xmin <= x <= xmax, with x^y exact */
  /* write x = m*2^e with m odd, m >= 3, m = k^(2^f) */
  /* we should have both k^(2^f) < 2^24 and k^n < 2^(24+mid) */
  /* compute the maximum odd k such that k^(2^f) < 2^24 */
  mpfr_set_ui_2exp(z, 1, 53, MPFR_RNDN);
  mpfr_nextbelow(z);
  mpfr_rootn_ui(z, z, F, MPFR_RNDD);
  assert(mpfr_fits_slong_p(z, MPFR_RNDD));
  long maxk = mpfr_get_ui(z, MPFR_RNDD);
  /* compute the maximum odd k such that k^n < 2^(24+mid) */
  mpfr_set_ui_2exp(z, 1, 53 + mid, MPFR_RNDN);
  mpfr_nextbelow(z);
  mpfr_rootn_ui(z, z, n, MPFR_RNDD);
  assert(mpfr_fits_slong_p(z, MPFR_RNDD));
  long maxk2 = mpfr_get_ui(z, MPFR_RNDD);
  maxk = (maxk < maxk2) ? maxk : maxk2;
  /* Write x=m*2^e, we should have m=k^(2^f) and e multiple of 2^f,
     then x^y = k^n*2^(e*n/2^f).
     We should have m=k^(2^f) with k <= kmax. */
  /* since m*2^e <= xmax, we have 2^e <= xmax/m <= xmax/3 */
  mpfr_set_d(z, xmax, MPFR_RNDD);
  mpfr_div_ui(z, z, 3, MPFR_RNDD);
  emax = mpfr_get_exp(z) - 1;
  /* for the minimal exponent, since x is an odd multiple of 2^e,
     x^y is an odd multiple of 2^(y*e), thus we want e >= -149 and y*e >=
     -149-mid */
  emin = -((e_min + mid) / y);
  if (emin < -e_min)
    emin = -e_min; /* so that x is representable */
  /* we should have e multiple of F */
  while (emin % F)
    emin++;

#pragma omp parallel for
  for (long e = emin; e <= emax; e += F) {
    for (long k = 3; k <= maxk; k += 2) {
      unsigned long m = k;
      for (long j = 0; j < f; j++)
        m = m * m;
      /* m (odd) should be less than 2^24 */
      assert(m < 0x40000000000000);
      mpfr_t zz;
      mpfr_init2(zz, 54);
      mpfr_set_ui_2exp(zz, m, e, MPFR_RNDN);
      if (mpfr_cmp_d(zz, xmin) >= 0 && mpfr_cmp_d(zz, xmax) <= 0) {
        check_sol(mpfr_get_d(zz, MPFR_RNDN), y);
        local_count++;
      }
      mpfr_clear(zz);
    }
  }
  mpfr_clear(z);
}

/* x = +/-2^e:
   for y integer, and -149-mid <= e*y <= 127, both x=2^e and -2^e are solutions
   for y=n/2^f for f>=1, n odd, we need e divisible by 2^f, and -149-mid <=
   e*y/2^f <= 127 */
static void check_pow2_x(long e) {
  unsigned long local_count = 0;
  if (e == 0) /* trivial solutions */
    return;

  /* case y integer */
  double x = ldexp(1.0, e);
  long ymin, ymax;
  if (e > 0) {
    ymin = -((e_min + mid) / e);
    ymax = e_max / e;
  } else /* e < 0 */
  {
    ymin = -(e_max / -e);
    ymax = (e_min + mid) / (-e);
  }
  for (long y = ymin; y <= ymax; y++)
    if (y != 0 && y != 1) {
      check_sol(x, (double)y);
      local_count++;
      check_sol(-x, (double)y);
      local_count++;
    }

  /* case y = n/2^f */
  int f = 1;
  while ((e % 2) == 0) {
    /* invariant: e = e_orig/2^f */
    e = e / 2;
    /* -149-mid <= e*y <= 127 */
    if (e > 0) {
      ymin = -((e_min + mid) / e);
      ymax = e_max / e;
    } else /* e < 0 */
    {
      ymin = -(e_max / -e);
      ymax = (e_min + mid) / (-e);
    }
    /* y should be odd */
    if ((ymin % 2) == 0)
      ymin++;
    for (long y = ymin; y <= ymax; y += 2) {
      check_sol(x, ldexp(y, -f));
      local_count++;
    }
    f++;
  }
}

int main(int argc, char *argv[]) {
  while (argc >= 2 && argv[1][0] == '-') {
    if (strcmp(argv[1], "-print") == 0) {
      print = 1;
      argv++;
      argc--;
    } else if (strcmp(argv[1], "-check") == 0) {
      check = 1;
      argv++;
      argc--;
    } else if (argc >= 3 && strcmp(argv[1], "-mid") == 0) {
      mid = atoi(argv[2]);
      argv += 2;
      argc -= 2;
    } else if (strcmp(argv[1], "-nosubnormal") == 0) {
      e_min = e_max + 1;
      argv++;
      argc--;
    } else {
      fprintf(stderr, "Error, unknown option %s\n", argv[1]);
      exit(1);
    }
  }

  for (rnd = 0; rnd < 4; rnd++) {
    printf("Check exact and midpoint values for round %s...", modes[rnd]);
    fflush(stdout);

    /* First deal with integer y >= 2. If x is not a power of 2, then y <= 35
       whatever the value of mid, since 3^34 has 54 bits, and 3^35 has 56 bits.
       Indeed, assume x = m*2^e with m odd, then m >= 3, thus we should have
       m^y < 2^(35+mid). */

#pragma omp parallel for
    for (int32_t y = 5; y < 35; y++)
      check_uint_y(y);

    /* Now deal with y = n/2^f for a positive integer f, and an odd n. If x is
       not a power of 2, assume x = m*2^e with m odd, m >= 3. Then m^(1/2^f)
       should be an odd integer >= 3, which implies f <= 5 whatever the value
       of mid, since 3^(2^5) has 51 bits, and 3^(2^6) has 102 bits. For the
       same reason as above, n <= 35. */

    for (int32_t f = 1; f < 6; f++)
      for (int32_t n = 1; n < 35; n += 2)
        if (n * f - 1)
          check_uint_2exp_y(f, n);

#pragma omp parallel for
    /* Now deal with x=2^e. */
    for (int e = -e_min; e <= e_max; e++)
      check_pow2_x(e);

    printf(" [ok]\n");
  }
}
