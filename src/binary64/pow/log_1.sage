# the following is a copy of the _INVERSE[] table in pow.h
INVERSE = ["0x1.69p+0", "0x1.67p+0", "0x1.65p+0", "0x1.63p+0", "0x1.61p+0", "0x1.5fp+0", "0x1.5ep+0", "0x1.5cp+0", "0x1.5ap+0", "0x1.58p+0", "0x1.56p+0", "0x1.54p+0", "0x1.53p+0", "0x1.51p+0", "0x1.4fp+0", "0x1.4ep+0", "0x1.4cp+0", "0x1.4ap+0", "0x1.48p+0", "0x1.47p+0", "0x1.45p+0", "0x1.44p+0", "0x1.42p+0", "0x1.4p+0", "0x1.3fp+0", "0x1.3dp+0", "0x1.3cp+0", "0x1.3ap+0", "0x1.39p+0", "0x1.37p+0", "0x1.36p+0", "0x1.34p+0", "0x1.33p+0", "0x1.32p+0", "0x1.3p+0", "0x1.2fp+0", "0x1.2dp+0", "0x1.2cp+0", "0x1.2bp+0", "0x1.29p+0", "0x1.28p+0", "0x1.27p+0", "0x1.25p+0", "0x1.24p+0", "0x1.23p+0", "0x1.21p+0", "0x1.2p+0", "0x1.1fp+0", "0x1.1ep+0", "0x1.1cp+0", "0x1.1bp+0", "0x1.1ap+0", "0x1.19p+0", "0x1.17p+0", "0x1.16p+0", "0x1.15p+0", "0x1.14p+0", "0x1.13p+0", "0x1.12p+0", "0x1.1p+0", "0x1.0fp+0", "0x1.0ep+0", "0x1.0dp+0", "0x1.0cp+0", "0x1.0bp+0", "0x1.0ap+0", "0x1.09p+0", "0x1.08p+0", "0x1.07p+0", "0x1.06p+0", "0x1.05p+0", "0x1.04p+0", "0x1.03p+0", "0x1.02p+0", "0x1.00p+0", "0x1.00p+0", "0x1.fdp-1", "0x1.fbp-1", "0x1.f9p-1", "0x1.f7p-1", "0x1.f5p-1", "0x1.f3p-1", "0x1.f1p-1", "0x1.fp-1", "0x1.eep-1", "0x1.ecp-1", "0x1.eap-1", "0x1.e8p-1", "0x1.e6p-1", "0x1.e5p-1", "0x1.e3p-1", "0x1.e1p-1", "0x1.dfp-1", "0x1.ddp-1", "0x1.dcp-1", "0x1.dap-1", "0x1.d8p-1", "0x1.d7p-1", "0x1.d5p-1", "0x1.d3p-1", "0x1.d2p-1", "0x1.dp-1", "0x1.cep-1", "0x1.cdp-1", "0x1.cbp-1", "0x1.c9p-1", "0x1.c8p-1", "0x1.c6p-1", "0x1.c5p-1", "0x1.c3p-1", "0x1.c2p-1", "0x1.cp-1", "0x1.bfp-1", "0x1.bdp-1", "0x1.bcp-1", "0x1.bap-1", "0x1.b9p-1", "0x1.b7p-1", "0x1.b6p-1", "0x1.b4p-1", "0x1.b3p-1", "0x1.b1p-1", "0x1.bp-1", "0x1.aep-1", "0x1.adp-1", "0x1.acp-1", "0x1.aap-1", "0x1.a9p-1", "0x1.a7p-1", "0x1.a6p-1", "0x1.a5p-1", "0x1.a3p-1", "0x1.a2p-1", "0x1.a1p-1", "0x1.9fp-1", "0x1.9ep-1", "0x1.9dp-1", "0x1.9cp-1", "0x1.9ap-1", "0x1.99p-1", "0x1.98p-1", "0x1.96p-1", "0x1.95p-1", "0x1.94p-1", "0x1.93p-1", "0x1.91p-1", "0x1.9p-1", "0x1.8fp-1", "0x1.8ep-1", "0x1.8dp-1", "0x1.8bp-1", "0x1.8ap-1", "0x1.89p-1", "0x1.88p-1", "0x1.87p-1", "0x1.86p-1", "0x1.84p-1", "0x1.83p-1", "0x1.82p-1", "0x1.81p-1", "0x1.8p-1", "0x1.7fp-1", "0x1.7ep-1", "0x1.7cp-1", "0x1.7bp-1", "0x1.7ap-1", "0x1.79p-1", "0x1.78p-1", "0x1.77p-1", "0x1.76p-1", "0x1.75p-1", "0x1.74p-1", "0x1.73p-1", "0x1.72p-1", "0x1.71p-1", "0x1.7p-1", "0x1.6fp-1", "0x1.6ep-1", "0x1.6dp-1", "0x1.6cp-1", "0x1.6bp-1", "0x1.6ap-1"]

# the following is a copy of the _LOG_INV[] table in pow.h
LOG_INV=[("-0x1.5ff3070a79p-2","-0x1.e9e439f105039p-45"),("-0x1.5a42ab0f4dp-2","0x1.e63af2df7ba69p-50"),("-0x1.548a2c3addp-2","-0x1.3167e63081cf7p-45"),("-0x1.4ec97326p-2","-0x1.34d7aaf04d104p-45"),("-0x1.4900680401p-2","0x1.8bccffe1a0f8cp-44"),("-0x1.432ef2a04fp-2","0x1.fb129931715adp-44"),("-0x1.404308686ap-2","-0x1.f8ef43049f7d3p-44"),("-0x1.3a64c55694p-2","-0x1.7a71cbcd735dp-44"),("-0x1.347dd9a988p-2","0x1.5594dd4c58092p-45"),("-0x1.2e8e2bae12p-2","0x1.67b1e99b72bd8p-45"),("-0x1.2895a13de8p-2","-0x1.a8d7ad24c13fp-44"),("-0x1.22941fbcf8p-2","0x1.a6976f5eb0963p-44"),("-0x1.1f8ff9e48ap-2","-0x1.7946c040cbe77p-45"),("-0x1.1980d2dd42p-2","-0x1.b7b3a7a361c9ap-45"),("-0x1.136870293bp-2","0x1.d3e8499d67123p-44"),("-0x1.1058bf9ae5p-2","0x1.4ab9d817d52cdp-44"),("-0x1.0a324e2739p-2","-0x1.c6bee7ef4030ep-47"),("-0x1.0402594b4dp-2","-0x1.036b89ef42d7fp-48"),("-0x1.fb9186d5e4p-3","0x1.d572aab993c87p-47"),("-0x1.f550a564b8p-3","0x1.323e3a09202fep-45"),("-0x1.e8c0252aa6p-3","0x1.6805b80e8e6ffp-45"),("-0x1.e27076e2bp-3","0x1.a342c2af0003cp-44"),("-0x1.d5c216b4fcp-3","0x1.1ba91bbca681bp-45"),("-0x1.c8ff7c79aap-3","0x1.7794f689f8434p-45"),("-0x1.c2968558c2p-3","0x1.cfd73dee38a4p-45"),("-0x1.b5b519e8fcp-3","0x1.4b722ec011f31p-44"),("-0x1.af3c94e80cp-3","0x1.a4e633fcd9066p-52"),("-0x1.a23bc1fe2cp-3","0x1.539cd91dc9f0bp-44"),("-0x1.9bb362e7ep-3","0x1.1f2a8a1ce0ffcp-45"),("-0x1.8e928de886p-3","-0x1.a8154b13d72d5p-44"),("-0x1.87fa06520cp-3","-0x1.22120401202fcp-44"),("-0x1.7ab890210ep-3","0x1.bdb9072534a58p-45"),("-0x1.740f8f5404p-3","0x1.0b66c99018aa1p-44"),("-0x1.6d60fe719ep-3","0x1.bc6e557134767p-44"),("-0x1.5ff3070a7ap-3","0x1.8586f183bebf2p-44"),("-0x1.59338d9982p-3","-0x1.0ba68b7555d4ap-48"),("-0x1.4ba36f39a6p-3","0x1.4354bb3f219e5p-44"),("-0x1.44d2b6ccb8p-3","0x1.70cc16135783cp-46"),("-0x1.3dfc2b0eccp-3","-0x1.8a72a62b8c13fp-45"),("-0x1.303d718e48p-3","0x1.680b5ce3ecb05p-50"),("-0x1.29552f82p-3","0x1.5b967f4471dfcp-44"),("-0x1.2266f190a6p-3","0x1.4d20ab840e7f6p-45"),("-0x1.1478584674p-3","-0x1.563451027c75p-46"),("-0x1.0d77e7cd08p-3","-0x1.cb2cd2ee2f482p-44"),("-0x1.0671512ca6p-3","0x1.a47579cdc0a3dp-45"),("-0x1.f0a30c0118p-4","0x1.d599e83368e91p-44"),("-0x1.e27076e2bp-4","0x1.a342c2af0003cp-45"),("-0x1.d4313d66ccp-4","0x1.9454379135713p-45"),("-0x1.c5e548f5bcp-4","-0x1.d0c57585fbe06p-46"),("-0x1.a926d3a4acp-4","-0x1.563650bd22a9cp-44"),("-0x1.9ab4246204p-4","0x1.8a64826787061p-45"),("-0x1.8c345d6318p-4","-0x1.b20f5acb42a66p-44"),("-0x1.7da766d7bp-4","-0x1.2cc844480c89bp-44"),("-0x1.60658a9374p-4","-0x1.0c3b1dee9c4f8p-44"),("-0x1.51b073f06p-4","-0x1.83f69278e686ap-44"),("-0x1.42edcbea64p-4","-0x1.bc0eeea7c9acdp-46"),("-0x1.341d7961bcp-4","-0x1.1d0929983761p-44"),("-0x1.253f62f0ap-4","-0x1.416f8fb69a701p-44"),("-0x1.16536eea38p-4","0x1.47c5e768fa309p-46"),("-0x1.f0a30c0118p-5","0x1.d599e83368e91p-45"),("-0x1.d276b8adbp-5","-0x1.6a423c78a64bp-46"),("-0x1.b42dd71198p-5","0x1.c827ae5d6704cp-46"),("-0x1.95c830ec9p-5","0x1.c148297c5feb8p-45"),("-0x1.77458f633p-5","0x1.181dce586af09p-44"),("-0x1.58a5bafc9p-5","0x1.b2b739570ad39p-45"),("-0x1.39e87b9fe8p-5","-0x1.eafd480ad9015p-44"),("-0x1.1b0d98924p-5","0x1.3401e9ae889bbp-44"),("-0x1.f829b0e78p-6","-0x1.980267c7e09e4p-45"),("-0x1.b9fc027bp-6","0x1.b9a010ae6922ap-44"),("-0x1.7b91b07d6p-6","0x1.3b955b602ace4p-44"),("-0x1.3cea44347p-6","0x1.6a2c432d6a40bp-44"),("-0x1.fc0a8b0fcp-7","-0x1.f1e7cf6d3a69cp-50"),("-0x1.7dc475f82p-7","0x1.eb1245b5da1f5p-44"),("-0x1.fe02a6b1p-8","-0x1.9e23f0dda40e4p-46"),("0","0"),("0","0"),("0x1.812121458p-8","0x1.ad50382973f27p-46"),("0x1.41929f968p-7","0x1.977c755d01368p-46"),("0x1.c317384c8p-7","-0x1.41f33fcefb9fep-44"),("0x1.228fb1feap-6","0x1.713e3284991fep-45"),("0x1.63d617869p-6","0x1.7abf389596542p-47"),("0x1.a55f548c6p-6","-0x1.de0709f2d03c9p-45"),("0x1.e72bf2814p-6","-0x1.8d75149774d47p-45"),("0x1.0415d89e78p-5","-0x1.dddc7f461c516p-44"),("0x1.252f32f8dp-5","0x1.83e9ae021b67bp-45"),("0x1.466aed42ep-5","-0x1.c167375bdfd28p-45"),("0x1.67c94f2d48p-5","0x1.dac20827cca0cp-44"),("0x1.894aa149f8p-5","0x1.9a19a8be97661p-44"),("0x1.aaef2d0fbp-5","0x1.0fc1a353bb42ep-45"),("0x1.bbcebfc69p-5","-0x1.7bf868c317c2ap-46"),("0x1.dda8adc68p-5","-0x1.1b1ac64d9e42fp-45"),("0x1.ffa6911ab8p-5","0x1.3008c98381a8fp-45"),("0x1.10e45b3cbp-4","-0x1.7cf69284a3465p-44"),("0x1.2207b5c784p-4","0x1.49d8cfc10c7bfp-44"),("0x1.2aa04a447p-4","0x1.7a48ba8b1cb41p-44"),("0x1.3bdf5a7d2p-4","-0x1.19bd0ad125895p-44"),("0x1.4d3115d208p-4","-0x1.53a2582f4e1efp-48"),("0x1.55e10050ep-4","0x1.c1d740c53c72ep-47"),("0x1.674f089364p-4","0x1.a79994c9d3302p-44"),("0x1.78d02263d8p-4","0x1.69b5794b69fb7p-47"),("0x1.8197e2f41p-4","-0x1.c0fe460d20041p-44"),("0x1.9335e5d594p-4","0x1.3115c3abd47dap-45"),("0x1.a4e7640b1cp-4","-0x1.e42b6b94407c8p-47"),("0x1.adc77ee5bp-4","-0x1.573b209c31904p-44"),("0x1.bf968769fcp-4","0x1.4218c8d824283p-45"),("0x1.d179788218p-4","0x1.36433b5efbeedp-44"),("0x1.da72763844p-4","0x1.a89401fa71733p-46"),("0x1.ec739830ap-4","0x1.11fcba80cdd1p-44"),("0x1.f57bc7d9p-4","0x1.76a6c9ea8b04ep-46"),("0x1.03cdc0a51ep-3","0x1.81a9cf169fc5cp-44"),("0x1.08598b59e4p-3","-0x1.7e5dd7009902cp-45"),("0x1.1178e8227ep-3","0x1.1ef78ce2d07f2p-45"),("0x1.160c8024b2p-3","0x1.ec2d2a9009e3dp-45"),("0x1.1f3b925f26p-3","-0x1.5f74e9b083633p-46"),("0x1.23d712a49cp-3","0x1.00d238fd3df5cp-46"),("0x1.2d1610c868p-3","0x1.39d6ccb81b4a1p-47"),("0x1.31b994d3a4p-3","0x1.f098ee3a5081p-44"),("0x1.3b08b6758p-3","-0x1.aade8f29320fbp-44"),("0x1.3fb45a5992p-3","0x1.19713c0cae559p-44"),("0x1.4913d8333cp-3","-0x1.53e43558124c4p-44"),("0x1.4dc7b897bcp-3","0x1.c79b60ae1ff0fp-47"),("0x1.5737cc9018p-3","0x1.9baa7a6b887f6p-44"),("0x1.5bf406b544p-3","-0x1.27023eb68981cp-46"),("0x1.6574ebe8c2p-3","-0x1.98c1d34f0f462p-44"),("0x1.6a399dabbep-3","-0x1.8f934e66a15a6p-44"),("0x1.6f0128b756p-3","0x1.577390d31ef0fp-44"),("0x1.7898d85444p-3","0x1.8e67be3dbaf3fp-44"),("0x1.7d6903caf6p-3","-0x1.4c06b17c301d7p-45"),("0x1.871213750ep-3","0x1.328eb42f9af75p-44"),("0x1.8beafeb39p-3","-0x1.73d54aae92cd1p-47"),("0x1.90c6db9fccp-3","-0x1.935f57718d7cap-46"),("0x1.9a8778debap-3","0x1.470fa3efec39p-44"),("0x1.9f6c40708ap-3","-0x1.337d94bcd3f43p-44"),("0x1.a454082e6ap-3","0x1.60a77c81f7171p-44"),("0x1.ae2ca6f672p-3","0x1.7a8d5ae54f55p-44"),("0x1.b31d8575bcp-3","0x1.c794e562a63cbp-44"),("0x1.b811730b82p-3","0x1.e90683b9cd768p-46"),("0x1.bd087383bep-3","-0x1.d4bc4595412b6p-45"),("0x1.c6ffbc6fp-3","0x1.ee138d3a69d43p-44"),("0x1.cc000c9db4p-3","-0x1.d6d585d57aff9p-46"),("0x1.d1037f2656p-3","-0x1.84a7e75b6f6e4p-47"),("0x1.db13db0d48p-3","0x1.2806a847527e6p-44"),("0x1.e020cc6236p-3","-0x1.52b00adb91424p-45"),("0x1.e530effe72p-3","-0x1.fdbdbb13f7c18p-44"),("0x1.ea4449f04ap-3","0x1.5e91663732a36p-44"),("0x1.f474b134ep-3","-0x1.bae49f1df7b5ep-44"),("0x1.f991c6cb3cp-3","-0x1.90d04cd7cc834p-44"),("0x1.feb2233eap-3","0x1.f3418de00938bp-45"),("0x1.01eae5626cp-2","0x1.a43dcfade85aep-44"),("0x1.047e60cde8p-2","0x1.dbdf10d397f3cp-45"),("0x1.09aa572e6cp-2","0x1.b50a1e1734342p-44"),("0x1.0c42d67616p-2","0x1.7188b163ceae9p-45"),("0x1.0edd060b78p-2","0x1.019b52d8435f5p-47"),("0x1.1178e8227ep-2","0x1.1ef78ce2d07f2p-44"),("0x1.14167ef367p-2","0x1.e0c07824daaf5p-44"),("0x1.16b5ccbadp-2","-0x1.23299042d74bfp-44"),("0x1.1bf99635a7p-2","-0x1.1ac89575c2125p-44"),("0x1.1e9e16788ap-2","-0x1.82eaed3c8b65ep-44"),("0x1.214456d0ecp-2","-0x1.caf0428b728a3p-44"),("0x1.23ec5991ecp-2","-0x1.6dbe448a2e522p-44"),("0x1.269621134ep-2","-0x1.1b61f10522625p-44"),("0x1.2941afb187p-2","-0x1.210c2b730e28bp-44"),("0x1.2bef07cdc9p-2","0x1.a9cfa4a5004f4p-45"),("0x1.314f1e1d36p-2","-0x1.8e27ad3213cb8p-45"),("0x1.3401e12aedp-2","-0x1.17c73556e291dp-44"),("0x1.36b6776be1p-2","0x1.16ecdb0f177c8p-46"),("0x1.396ce359bcp-2","-0x1.5839c5663663dp-47"),("0x1.3c25277333p-2","0x1.83b54b606bd5cp-46"),("0x1.3edf463c17p-2","-0x1.f067c297f2c3fp-44"),("0x1.419b423d5fp-2","-0x1.ce379226de3ecp-44"),("0x1.44591e053ap-2","-0x1.6e95892923d88p-47"),("0x1.4718dc271cp-2","0x1.06c18fb4c14c5p-44"),("0x1.49da7f3bccp-2","0x1.07b334daf4b9ap-44"),("0x1.4c9e09e173p-2","-0x1.e20891b0ad8a4p-45"),("0x1.4f637ebbaap-2","-0x1.fc158cb3124b9p-44"),("0x1.522ae0738ap-2","0x1.ebe708164c759p-45"),("0x1.54f431b7bep-2","0x1.a8954c0910952p-46"),("0x1.57bf753c8dp-2","0x1.fadedee5d40efp-46"),("0x1.5a8cadbbeep-2","-0x1.7c79b0af7ecf8p-48"),("0x1.5d5bddf596p-2","-0x1.a0b2a08a465dcp-47"),("0x1.602d08af09p-2","0x1.ebe9176df3f65p-46"),("0x1.630030b3abp-2","-0x1.db623e731aep-45")]

# this routine is used in log1_case2()
def p_1(z,verbose=false):
   R = z.parent()
   p3 = R("0x1.5555555555558p-2",16)
   p4 = R("-0x1.0000000000003p-2",16)
   p5 = R("0x1.999999981f535p-3",16)
   p6 = R("-0x1.55555553d1eb4p-3",16)
   p7 = R("0x1.2494526fd4a06p-3",16)
   p8 = R("-0x1.0001f0c80e8cep-3",16)
   Z = z.exact_rational()
   p = -Z^2/2+p3.exact_rational()*Z^3+p4.exact_rational()*Z^4
   p += p5.exact_rational()*Z^5+p6.exact_rational()*Z^6
   p += p7.exact_rational()*Z^7+p8.exact_rational()*Z^8
   err0 = n(p-(log(1+Z)-Z),200)
   if verbose:
      print ("err0=", err0)
   wh = z*z
   wl = fma(z,z,-wh)
   t = fma(p8,z,p7)
   err1 = p8.exact_rational()*Z+p7.exact_rational()-t.exact_rational()
   err1 = n(err1*Z^7,200)
   if verbose:
      print ("err1=", err1)
   u = fma(p6,z,p5)
   err2 = p6.exact_rational()*Z+p5.exact_rational()-u.exact_rational()
   err2 = n(err2*Z^5,200)
   if verbose:
      print ("err2=", err2)
   v = fma(p4,z,p3)
   err3 = p4.exact_rational()*Z+p3.exact_rational()-v.exact_rational()
   err3 = n(err3*Z^3,200)
   if verbose:
      print ("err3=", err3)
   u0 = u
   u = fma(t,wh,u)
   err4 = t.exact_rational()*wh.exact_rational()+u0.exact_rational()-u.exact_rational()
   err4 = abs(err4) + abs(t.exact_rational()*wl.exact_rational())
   err4 = n(err4*Z^5,200)
   if verbose:
      print ("err4=", err4)
   v0 = v
   v = fma(u,wh,v)
   err5 = u.exact_rational()*wh.exact_rational()+v0.exact_rational()-v.exact_rational()
   err5 = abs(err5) + abs(u.exact_rational()*wl.exact_rational())
   err5 = n(err5*Z^3,200)
   if verbose:
      print ("err5=", err5)
   u = v*wh
   err6 = v.exact_rational()*wh.exact_rational()-u.exact_rational()
   err6 = n(err6*Z,200)
   if verbose:
      print ("err6=", err6)
   hi = R(-0.5)*wh
   tmp = R(-0.5)*wl
   lo = fma(u,z,R(-0.5)*wl)
   err7 = u.exact_rational()*Z+tmp.exact_rational()-lo.exact_rational()
   err7 = n(err7,200)
   if verbose:
      print ("err7=", err7)
   return hi,lo

# z=RR("0x1.00fc8ff201f86p-8",16)
# get_err_p_1(z,rnd='RNDU')
# 9.7218108948365964683321945268439708945084471028840037531333e-24
# z=RR("-0x1.07fc623a1p-8",16)
# get_err_p_1(z,rnd='RNDN')
# -8.11281399540949e-6 -2.18522070322654e-8
def get_err_p_1(z,rnd='RNDN'):
   R = RealField(53,rnd=rnd)
   z = R(z)
   hi,lo = p_1(z)
   Z = z.exact_rational()
   print (hi, lo)
   err = n(hi.exact_rational()+lo.exact_rational()-(log(1+Z)-Z),200)
   return err, log(abs(err))/log(2.)

# return the 'ulp' of the interval x, i.e., max(ulp(t)) for t in x
# this internal routine is used below
def RIFulp(x):
   return max(x.lower().ulp(),x.upper().ulp())

# return (err,hi,lo) where for z in [zmin,zmax]:
# - err is the maximal absolute error on p_1()
# - hi is the interval for the hi value in p_1()
# - lo is the interval for the lo value in p_1()
# analyze_p1(-0.0040283203125,0.0040283203125)
# (1.88089003891643e-23, 0.00000?, 0.?e-7)
# if rel is true, returns the relative error instead
# (then we also take into account the z term, not computed by p_1())
# this internal routine is used in analyze_p1_all()
def analyze_p1(zmin,zmax,verbose=true,rel=false):
    R.<x> = RealField(53)[]
    p = x-x^2/2+RR("0x1.5555555555558p-2",16)*x^3+RR("-0x1.0000000000003p-2",16)*x^4+RR("0x1.999999981f535p-3",16)*x^5+RR("-0x1.55555553d1eb4p-3",16)*x^6+RR("0x1.2494526fd4a06p-3",16)*x^7+RR("-0x1.0001f0c80e8cep-3",16)*x^8
    p = [p[i] for i in [3..8]] # coefficients of degree 1 and 2 are fixed
    err0 = 2^-81.63 # maximal absolute error
    assert err0<=2^-81.63, "err0<=2^-81.63"
    err0_rel = 2^-72.423 # maximal relative error
    assert err0_rel<=2^-72.423, "err0_rel<=2^-72.423"
    # err0_rel is relative to log(1+z), convert to absolute error
    err0r = err0_rel*max(abs(log(1+zmin)),abs(log(1+zmax)))
    err0 = min(err0,err0r)
    z = RIF(zmin,zmax)
    # a_mul (&wh, &wl, z, z)
    wh = z*z
    wl = RIF(0,wh.upper().ulp())
    # t = __builtin_fma (P_1[5], z, P_1[4])
    t = p[5]*z+p[4]
    err1 = RIFulp(t)
    # err1 is multiplied by z^7
    err1 = err1*z.abs().upper()^7
    if verbose:
       print ("err1=", log(err1)/log(2.))
    # u = __builtin_fma (P_1[3], z, P_1[2])
    u = p[3]*z+p[2]
    err2 = RIFulp(u)
    # err2 is multiplied by z^5
    err2 = err2*z.abs().upper()^5
    if verbose:
       print ("err2=", log(err2)/log(2.))
    # v = __builtin_fma (P_1[1], z, P_1[0])
    v = p[1]*z+p[0]
    err3 = RIFulp(v)
    # err3 is multiplied by z^3
    err3 = err3*z.abs().upper()^3
    if verbose:
       print ("err3=", log(err3)/log(2.))
    # u = __builtin_fma (t, wh, u)
    u = t*wh+u
    err4a = RIFulp(u) # rounding error in fma
    err4b= (t*wl).abs().upper() # neglected term t*wl
    err4 = (err4a+err4b)*z.abs().upper()^5
    if verbose:
       print ("err4=", log(err4)/log(2.))
    # v = __builtin_fma (u, wh, v)
    v = u*wh+v
    err5a = RIFulp(v) # rounding error in fma
    err5b = (u*wl).abs().upper() # neglected term u*wl
    err5 = (err5a+err5b)*z.abs().upper()^3
    if verbose:
       print ("err5=", log(err5)/log(2.))
    # u = v * wh
    u = v*wh
    err6a = RIFulp(u)
    err6b = (v*wl).abs().upper() # neglected term v*wl
    err6 = (err6a+err6b)*z.abs().upper()
    if verbose:
       print ("err6=", log(err6)/log(2.))
    hi = -wh/2 # exact
    # *lo = __builtin_fma (u, z, -0.5 * wl)
    lo = u*z-wl/2
    err7 = RIFulp(lo)
    if verbose:
       print ("err7=", log(err7)/log(2.))
    if rel: # convert into absolute error for err0
       err0 = err0*(hi+lo).abs().upper()
    if verbose:
       print ("err0=", log(err0)/log(2.))
    err = err0+err1+err2+err3+err4+err5+err6+err7
    if verbose:
       print ("err=", log(err)/log(2.))
    if rel:
       err = err/(z+hi+lo) # take into account the z term
       err = err.abs().upper()
    if verbose:
       print ("err=", log(err)/log(2.))
    return err, hi, lo

# return (maxerr, maxhi, maxlo) where maxerr is the maximal absolute error
# (or relative if rel=true) for p_1(), maxhi is the maximal value of |hi|,
# and maxlo the maximal value of |lo|
# If rel=true, the relative error is with respect to z+p_1(), where the z term
# is not computed by p_1().
# Algorithm: divide each binade in 2^k,
# knowing that z in an integer multiple of 2^-61
# analyze_p1_all(k=8)
# zmin= -0x1.08p-8 zmax= -0x1.07p-8 ratio= 0.00271421004672723
# (1.86643537965471e-23, 8.11368227005005e-6, 2.18557187731001e-8)
# analyze_p1_all(k=8,rel=true)
# zmin= -0x1.08p-8 zmax= -0x1.07p-8 ratio= 0.00271421004672723
# (4.62287071580535e-21, 8.11368227005005e-6, 2.18557187731001e-8) # 2^-67.55
# this routine is used for the error analysis of p_1()
def analyze_p1_all(k=0,rel=false):
   maxerr = 0
   maxhi = 0
   maxlo = 0
   maxratio = 0 # ratio lo/hi
   for e in range(-7,-61,-1):
      # consider 2^(e-1) <= |z| < 2^e
      Zmin = RR(2^(e-1))
      Zmax = RR(2^e)
      h = (Zmax-Zmin)/2^k
      for i in range(2^k):
         zmin = Zmin+i*h
         zmax = zmin+h
         if zmin>=0.0040283203125:
            continue
         if zmax>0.0040283203125:
            zmax=0.0040283203125
         err, hi, lo = analyze_p1(zmin,zmax,rel=rel,verbose=false)
         # err2, _, _ = analyze_p1(zmin,zmax,rel=rel,verbose=false)
         # err = min(err,err2)
         ratio = lo.abs().upper()/hi.abs().lower()
         hi = hi.abs().upper()
         lo = lo.abs().upper()
         if err>maxerr:
            print ("zmin=", get_hex(zmin), "zmax=", get_hex(zmax), "err=", err)
            maxerr = err
         if hi>maxhi:
            print ("zmin=", get_hex(zmin), "zmax=", get_hex(zmax), "hi=", hi)
            maxhi = hi
         if lo>maxlo:
            print ("zmin=", get_hex(zmin), "zmax=", get_hex(zmax), "lo=", lo)
            maxlo = lo
         if ratio>maxratio:
            print ("zmin=", get_hex(zmin), "zmax=", get_hex(zmax), "ratio=", ratio)
            maxratio = ratio
         err, hi, lo = analyze_p1(-zmax,-zmin,rel=rel,verbose=false)
         ratio = lo.abs().upper()/hi.abs().lower()
         hi = hi.abs().upper()
         lo = lo.abs().upper()
         if err>maxerr:
            print ("zmin=", get_hex(-zmax), "zmax=", get_hex(-zmin), "err=", err)
            maxerr = err
         if hi>maxhi:
            print ("zmin=", get_hex(-zmax), "zmax=", get_hex(-zmin), "hi=", hi)
            maxhi = hi
         if lo>maxlo:
            print ("zmin=", get_hex(-zmax), "zmax=", get_hex(-zmin), "lo=", lo)
            maxlo = lo 
         if ratio>maxratio:
            print ("zmin=", get_hex(-zmax), "zmax=", get_hex(-zmin), "ratio=", ratio)
            maxratio = ratio
   return maxerr, maxhi, maxlo

# analyze log_1() for -1074 <= E <= 1024, E<>0, and 181 <= i <= 362
# return (err,hi,lo) where err is the base-2 logarithm of the maximal
# absolute error of log_1() for values E and i, hi is the interval for the
# variable hi in log_1(), and lo is the interval for the variable lo
# if rel is true, returns the base-2 logarithm of the relative error instead
# analyze_log1_cancel0(-1074,181)
# (2.54645781701655e-23, -744.79?, 0.?e-7)
# analyze_log1_cancel0(-1074,181,rel=true)
# (3.41906819097493e-26, -744.79?, 0.?e-7)
# internal routine used in analyze_log1_cancel0_all()
def analyze_log1_cancel0(E,i,verbose=true,rel=false,tmin=None,tmax=None):
   assert E!=0, "E!=0"
   err0 = 2^-75.492 # absolute error bound for p_1()
   E = RIF(E)
   t0 = RR("0x1.6a09e667f3bcdp-1",16)
   t1 = 2*t0
   # we have i/256 <= t < (i+1)/256
   if tmin==None:
      tmin = RR(i/256)
      tmin = max(tmin,t0)
   if tmax==None:
      tmax = RR((i+1)/256)
      tmax = min(tmax,t1)
   t = RIF(tmin,tmax)
   r = RR(INVERSE[i-181],16)
   l1 = RR(LOG_INV[i-181][0],16)
   l2 = RR(LOG_INV[i-181][1],16)
   R = r.exact_rational()
   L1 = l1.exact_rational()
   L2 = l2.exact_rational()
   err7 = abs(n(log(R)+L1+L2,200))
   if verbose:
      print ("err7:", log(err7)/log(2.))
   assert err7<=2^-97, "err7<=2^-97"
   # z = __builtin_fma(r,t,-1)
   zlo = fma(r,RR(t.lower()),RR(-1))
   zup = fma(r,RR(t.upper()),RR(-1))
   z = RIF(zlo,zup)
   # we know |z| <= 0.0040283203125
   zmax = 0.0040283203125
   zlo = max(z.lower(),-zmax)
   zhi = min(z.upper(),zmax)
   z = RIF(zlo,zhi)
   assert z.abs().upper()<=0.0040283203125,"z.abs().upper()<=0.0040283203125"
   LOG2_H = RR("0x1.62e42fefa38p-1",16)
   LOG2_L = RR("0x1.ef35793c7673p-45",16)
   err8 = abs(n(log(2)-LOG2_H.exact_rational()-LOG2_L.exact_rational(),200))
   assert err8<=1.95e-31, "err8<=1.95e-31"
   err8 = (E*err8).abs().upper()
   if verbose:
      print ("err8:", log(err8)/log(2.))
   assert err8<=2^-91.9, "err8<=2^-91.9"
   Elog2 = E*(LOG2_H+LOG2_L)
   assert Elog2.abs().lower()>0.6931,"Elog2.abs().lower()>0.6931"
   logr = RIF(l1)+RIF(l2)
   assert logr.abs().upper()<0.3467,"logr.abs().upper()<0.3467"
   # th = __builtin_fma (E, LOG2_H, l1)
   th = E*LOG2_H+RIF(l1) # exact
   # tl = __builtin_fma (E, LOG2_L, l2)
   tl = E*LOG2_L+RIF(l2)
   err1 = RIFulp(tl)
   if verbose:
      print ("err1:", log(err1)/log(2.))
   assert err1<=2^-86, "err1<=2^-86"
   # fast_sum (hi, lo, th, z, tl)
   #    fast_two_sum (hi, lo, th, z) # exact
   hi = th+z
   u = RIFulp(hi)
   lo = RIF(-u,u)
   err2 = 2^-105*hi.abs().upper()
   #    lo += tl
   lo = lo+tl
   err3 = RIFulp(lo)
   if verbose:
      print ("err3:", log(err3)/log(2.))
   assert err3<=2^-86, "err3<=2^-86"
   # p_1 (&ph, &pl, z)
   ph = RIF(-8.2e-6,8.2e-6)
   pl = RIF(-2.2e-8,2.2e-8)
   log1pz = z+ph+pl
   assert log1pz.abs().upper()<0.0041,"log1pz.abs().upper()<0.0041"
   # fast_sum (hi, lo, *hi, ph, *lo + pl)
   #    fast_two_sum (hi, lo, hi_in, ph)
   hi = hi + ph
   err4 = 2^-105*hi.abs().upper()
   if verbose:
      print ("err4:", log(err4)/log(2.))
   tmp = lo + pl
   err5 = RIFulp(tmp)
   if verbose:
      print ("err5:", log(err5)/log(2.))
   assert err5<=2^-78, "err5<=2^-78"
   u = RIFulp(hi)
   lo = RIF(-u,u)
   #    lo += lo_in + pl
   lo += tmp
   err6 = RIFulp(lo)
   if verbose:
      print ("err6:", log(err6)/log(2.))
   assert err4+err6<=2^-77.99, "err4+err6<=2^-77.99"
   err = err0+err1+err2+err3+err4+err5+err6+err7+err8
   if verbose:
      print ("err:", log(err)/log(2.))
   assert err<=2^-74.87, "err<=2^-74.87"
   if rel==true:
      if verbose:
         print ("lower(hi+lo)=", (hi+lo).abs().lower())
      err = err/(hi+lo).abs().lower()
   return err, hi, lo

def fast_two_sum(a,b):
   hi = a + b
   e = hi - a
   lo = b-e
   return hi,lo

def fast_sum(a,bh,bl):
   hi, lo = fast_two_sum (a, bh)
   lo += bl
   return hi,lo

# hi,lo=simulate_log1_cancel0(1,187,RR("0x1.77fe66e2effcdp-1",16))
def simulate_log1_cancel0(E,i,t):
   R = t.parent()
   assert E!=0, "E!=0"
   E = R(E)
   t = R(t)
   r = R(INVERSE[i-181],16)
   l1 = R(LOG_INV[i-181][0],16)
   l2 = R(LOG_INV[i-181][1],16)
   # z = __builtin_fma(r,t,-1)
   z = fma(r,t,R(-1))
   LOG2_H = R("0x1.62e42fefa38p-1",16)
   LOG2_L = R("0x1.ef35793c7673p-45",16)
   # th = __builtin_fma (E, LOG2_H, l1)
   th = fma(E,LOG2_H,l1)
   # tl = __builtin_fma (E, LOG2_L, l2)
   tl = fma(E,LOG2_L,l2)
   # fast_sum (hi, lo, th, z, tl)
   hi, lo = fast_sum (th, z, tl)
   #    lo += tl
   lo += tl
   # p_1 (&ph, &pl, z)
   ph, pl = p_1 (z)
   # fast_sum (hi, lo, *hi, ph, *lo + pl)
   hi, lo = fast_sum (hi, ph, lo+pl)
   return hi, lo

# try to find a large value of |lo/hi| for cancel0
# analyze_log1_cancel0_aux(-1,362)
# x= 0x1.6a00000193236p-1 |lo/hi|= 9.99655510197488e-12
# analyze_log1_cancel0_aux(1,187)
# x= 0x1.77fffffec9d58p+0 |lo/hi|= 5.65121653260885e-8
def analyze_log1_cancel0_aux(E,i,rnd='RNDN'):
   R = RealField(53,rnd=rnd)
   t0 = R("0x1.6a09e667f3bcdp-1",16)
   t1 = 2*t0
   tmin = max(t0,R(i/256))
   tmax = min(t1,R((i+1)/256)).nextbelow()
   max_ratio = 0
   while true:
      t = R.random_element(tmin,tmax)
      hi, lo = simulate_log1_cancel0(E,i,t)
      ratio = abs(lo/hi)
      if ratio > max_ratio:
         max_ratio = ratio
         print ("x=", get_hex(t*2^E), "|lo/hi|=", ratio)

def a_mul(a,b):
   hi = a * b
   lo = fma (a, b, -hi)
   return hi,lo

# try to find a large value of rl in mul_1()
# analyze_log1_cancel0_aux2(1,187)
# x= 0x1.77ffffeeb6e34p+0 y= 0x1.e498fc56e3761p+10 rl: -14.5354985177154
def analyze_log1_cancel0_aux2(E,i,rnd='RNDN'):
   R = RealField(53,rnd=rnd)
   t0 = R("0x1.6a09e667f3bcdp-1",16)
   t1 = 2*t0
   tmin = max(t0,R(i/256))
   tmax = min(t1,R((i+1)/256)).nextbelow()
   max_rl = 0
   while true:
      t = R.random_element(tmin,tmax)
      hi, lo = simulate_log1_cancel0(E,i,t)
      # find the largest y such that |y*h| <= 745.1399
      ymax = R(745.1399)/abs(hi)
      rh, s = a_mul(ymax,hi)
      rl = fma(ymax,lo,s)
      if abs(rl) > max_rl:
         max_rl = abs(rl)
         print ("x=", get_hex(t*2^E), "y=", get_hex(ymax), "rl:", log(max_rl)/log(2.))
      ymax = -ymax
      rh, s = a_mul(ymax,hi)
      rl = fma(ymax,lo,s)
      if abs(rl) > max_rl:
         max_rl = abs(rl)
         print ("x=", get_hex(t*2^E), "y=", get_hex(ymax), "rl:", log(max_rl)/log(2.))

# same as analyze_log1_cancel0_aux2, but instead of generating random
# elements in [tmin,tmax], start by both ends
# analyze_log1_cancel0_aux3(1,187,rnd='RNDZ')
# x= 0x1.77ffffffffc75p+0 y= 0x1.e498fc1cf04c1p+10 rl: -14.5354955646868
def analyze_log1_cancel0_aux3(E,i,rnd='RNDN'):
   R = RealField(53,rnd=rnd)
   t0 = R("0x1.6a09e667f3bcdp-1",16)
   t1 = 2*t0
   tmin = max(t0,R(i/256))
   tmax = min(t1,R((i+1)/256)).nextbelow()
   max_rl = 0
   while true:
      t = tmin
      hi, lo = simulate_log1_cancel0(E,i,t)
      ymax = R(745.1399)/abs(hi)
      rh, s = a_mul(ymax,hi)
      rl = fma(ymax,lo,s)
      if abs(rl) > max_rl:
         max_rl = abs(rl)
         print ("x=", get_hex(t*2^E), "y=", get_hex(ymax), "rl:", log(max_rl)/log(2.))
      ymax = -ymax
      rh, s = a_mul(ymax,hi)
      rl = fma(ymax,lo,s)
      if abs(rl) > max_rl:
         max_rl = abs(rl)
         print ("x=", get_hex(t*2^E), "y=", get_hex(ymax), "rl:", log(max_rl)/log(2.))
      t = tmax
      hi, lo = simulate_log1_cancel0(E,i,t)
      ymax = R(745.1399)/abs(hi)
      rh, s = a_mul(ymax,hi)
      rl = fma(ymax,lo,s)
      if abs(rl) > max_rl:
         max_rl = abs(rl)
         print ("x=", get_hex(t*2^E), "y=", get_hex(ymax), "rl:", log(max_rl)/log(2.))
      ymax = -ymax
      rh, s = a_mul(ymax,hi)
      rl = fma(ymax,lo,s)
      if abs(rl) > max_rl:
         max_rl = abs(rl)
         print ("x=", get_hex(t*2^E), "y=", get_hex(ymax), "rl:", log(max_rl)/log(2.))
      tmin = tmin.nextabove()
      tmax = tmax.nextbelow()

# analyze_log1_cancel0_all()
# E= -1074 i= 299 err= -75.0558540278622
# E= -1 i= 362 ratio= 6.34803960455073e-8
# (2.54645840985511e-23, 744.786649588269, 2.20593882521655e-8, 6.34803960455073e-8)
# analyze_log1_cancel0_all(rel=true)
# E= -1 i= 362 err= -73.5285314832271
# E= -1 i= 362 ratio= 6.34803960455073e-8
# (7.34017622028180e-23, 744.786649588269, 2.20593882521655e-8, 6.34803960455073e-8)
# this routine is used in the error analysis of log_1()
def analyze_log1_cancel0_all(rel=false):
   maxerr = 0
   maxhi = 0
   maxlo = 0
   maxratio = 0
   for E in [-1074..1024]:
      if E==0:
         continue
      for i in range(362,180,-1):
         err, hi, lo = analyze_log1_cancel0(E,i,rel=rel,verbose=false)
         if err>maxerr:
            print ("E=", E, "i=", i, "err=", log(err)/log(2.))
            maxerr = err
         if hi.abs().upper()>maxhi:
            maxhi = hi.abs().upper()
         if lo.abs().upper()>maxlo:
            maxlo = lo.abs().upper()
         ratio = lo.abs().upper()/hi.abs().lower()
         if ratio>maxratio:
            print ("E=", E, "i=", i, "ratio=", ratio)
            maxratio = ratio
   return maxerr,maxhi,maxlo,maxratio
         
# analyze log_1() for E=0 and tmin <= t < tmax
# return (err,maxhi,maxlo) where err is the base-2 logarithm of the maximal
# absolute error of log_1() for i, maxhi is the maximal absolute
# error of hi, and maxlo is the maximal absolute error of lo
# if rel is true, returns the base-2 logarithm of the relative error instead
# analyze_log1_cancel1(RR(181/256),RR(182/256))
# (2.54384892790820e-23, -0.35?, 0.?e-16)
# analyze_log1_cancel1(RR(181/256),RR(182/256),rel=true)
# (7.45648347508473e-23, -0.35?, 0.?e-16)
# internal routine used in analyze_log1_cancel1_all()
def analyze_log1_cancel1(tmin,tmax,verbose=true,rel=false):
   err0 = 2^-75.492 # absolute error bound for p_1()
   t0 = RR("0x1.6a09e667f3bcdp-1",16)
   t1 = 2*t0
   tmin = max(tmin,t0)
   tmax = min(tmax,t1)
   if tmax<tmin:
      return 0, RIF(0), RIF(0)
   i = ZZ(floor(tmin*256))
   i1 = ZZ(floor(tmax.nextbelow()*256))
   assert i==i1,"i==i1"
   t = RIF(tmin,tmax)
   r = RR(INVERSE[i-181],16)
   l1 = RR(LOG_INV[i-181][0],16)
   l2 = RR(LOG_INV[i-181][1],16)
   R = r.exact_rational()
   L1 = l1.exact_rational()
   L2 = l2.exact_rational()
   err7 = abs(n(log(R)+L1+L2,200))
   if verbose:
      print ("err7:", log(err7)/log(2.))
   assert err7<=2^-97, "err7<=2^-97"
   # z = __builtin_fma(r,t,-1)
   # z = RIF(r)*t-RIF(1)
   # t.lower() has rounding RNDD -> convert to RR, same for t.upper()
   zlo = fma(r,RR(t.lower()),RR(-1))
   zup = fma(r,RR(t.upper()),RR(-1))
   z = RIF(zlo,zup)
   # we know |z| <= 0.0040283203125
   zmax = 0.0040283203125
   assert z.abs().upper()<=zmax,"z.abs().upper()<=zmax"
   LOG2_H = RR("0x1.62e42fefa38p-1",16)
   LOG2_L = RR("0x1.ef35793c7673p-45",16)
   err8 = abs(n(log(2)-LOG2_H.exact_rational()-LOG2_L.exact_rational(),200))
   assert err8<=1.95e-31, "err8<=1.95e-31"
   err8 = 0
   logr = RIF(l1)+RIF(l2)
   assert logr.abs().upper()<0.3467,"logr.abs().upper()<0.3467"
   # th = __builtin_fma (E, LOG2_H, l1) -> th = l1
   th = RIF(l1) # exact
   # tl = __builtin_fma (E, LOG2_L, l2) -> tl = l2
   tl = RIF(l2)
   err1 = 0
   # fast_sum (hi, lo, th, z, tl)
   #    fast_two_sum (hi, lo, th, z) # exact
   hi = th+z
   u = RIFulp(hi)
   lo = RIF(-u,u)
   err2 = 2^-105*hi.abs().upper()
   #    lo += tl
   lo = lo+tl
   err3 = RIFulp(lo)
   if verbose:
      print ("err3:", log(err3)/log(2.))
   assert err3<=2^-86, "err3<=2^-86"
   # p_1 (&ph, &pl, z)
   ph = RIF(-8.2e-6,8.2e-6)
   pl = RIF(-2.2e-8,2.2e-8)
   log1pz = z+ph+pl
   assert log1pz.abs().upper()<0.0041,"log1pz.abs().upper()<0.0041"
   # fast_sum (hi, lo, *hi, ph, *lo + pl)
   #    fast_two_sum (hi, lo, hi_in, ph)
   hi = hi + ph
   err4 = 2^-105*hi.abs().upper()
   if verbose:
      print ("err4:", log(err4)/log(2.))
   tmp = lo + pl
   err5 = RIFulp(tmp)
   if verbose:
      print ("err5:", log(err5)/log(2.))
   assert err5<=2^-77, "err5<=2^-77"
   u = RIFulp(hi)
   lo = RIF(-u,u)
   #    lo += lo_in + pl
   lo += tmp
   err6 = RIFulp(lo)
   if verbose:
      print ("err6:", log(err6)/log(2.))
   assert err4+err6<=2^-77.99, "err4+err6<=2^-77.99"
   # fast_two_sum (hi, lo, *hi, *lo)
   hi_in = hi
   hi = hi_in+lo
   u = RIFulp(hi)
   lo = RIF(-u,u)
   err9 = 2^-105*hi.abs().upper()
   if verbose:
      print ("err9:", log(err9)/log(2.))
   err = err0+err1+err2+err3+err4+err5+err6+err7+err8+err9
   if verbose:
      print ("err:", log(err)/log(2.))
   assert err<=2^-74.87, "err<=2^-74.87"
   if rel==true:
      if verbose:
         print ("lower(hi+lo)=", (hi+lo).abs().lower())
      err = err/(hi+lo).abs().lower()
   return err, hi, lo

# analyze the maximal error for case E=0
# if k>0, subdivide each interval [i/256,(i+1)/256) in 2^k sub-intervals
# analyze_log1_cancel1_all()
# i= 299 j= 0 tmin= 0x1.2bp+0 tmax= 0x1.2cp+0 err= -75.0573324921357
# (2.54385015291965e-23, 0.346581817985340, 5.55111512312579e-17, 4.45007874687502e-16)
# analyze_log1_cancel1_all(k=8)
# i= 299 j= 255 tmin= 0x1.2bffp+0 tmax= 0x1.2cp+0 err= -75.0573324921357
# (2.54385015291965e-23, 0.346581817985340, 5.55111512312579e-17, 2.22836215834445e-16)
# analyze_log1_cancel1_all(rel=true)
# i= 257 j= 0 tmin= 0x1.01p+0 tmax= 0x1.02p+0 err= -67.0521991647768
# (6.53546925853479e-21, 0.346581817985340, 5.55111512312579e-17, 4.45007874687502e-16)
# analyze_log1_cancel1_all(k=8,rel=true)
# i= 257 j= 0 tmin= 0x1.01p+0 tmax= 0x1.0101p+0 err= -67.0521991647930
# (6.53546925846129e-21, 0.346581817985340, 5.55111512312579e-17, 2.22836215834445e-16)
# this routine is used in the error analysis of log_1()
def analyze_log1_cancel1_all(k=0,rel=false):
   maxerr = 0
   maxhi = 0
   maxlo = 0
   maxratio = 0
   for i in [181..362]:
      if i in [255,256]:
         continue
      for j in range(2^k):
         tmin = RR((i+j/2^k)/256)
         tmax = RR((i+(j+1)/2^k)/256)
         err, hi, lo = analyze_log1_cancel1(tmin,tmax,rel=rel,verbose=false)
         if err>maxerr:
            print ("i=", i, "j=", j, "tmin=", get_hex(tmin), "tmax=", get_hex(tmax), "err=", log(err)/log(2.))
            maxerr = err
         if hi.abs().upper()>maxhi:
            maxhi = hi.abs().upper()
         if lo.abs().upper()>maxlo:
            maxlo = lo.abs().upper()
         ratio = lo.abs().upper()/hi.abs().lower()
         if ratio>maxratio:
            # print ("i=", i, "ratio=", ratio)
            maxratio = ratio
   return maxerr,maxhi,maxlo,maxratio

# case E=0 and i in {255,256}, thus 255/256 <= x < 257/256
# analyze_log1_cancel2(RR("0x1.0001p+0",16),RR("0x1.0002p+0",16),rel=true)
# ph= -4.65661287307740e-10 -1.16415321826934e-10
# pl= 1.18421078794122e-15 9.47379472854465e-15
# err4: -Infinity
# err6: -99.0000000000000
# err0: -96.0000152509636
# err: -95.8300885549618
# lower(hi+lo)= 0.0000152583234023968
# (9.30597844428014e-25, 0.0000305174617096782, 9.47379811667644e-15)
# tmin=RR("0x1.00e8p+0",16)
# analyze_log1_cancel2(tmin,tmin.nextabove(),rel=true)
# err4: -Infinity
# err6: -79.0000000000001
# err0: -76.1915735999048
# err: -75.9990624132060
# (3.74767649834811e-21, 0.00353377312421821, 1.47486264030454e-8)
# internal routine used in analyze_log1_cancel2_all()
def analyze_log1_cancel2(tmin,tmax,verbose=true,rel=false):
   assert tmin<tmax, "tmin<tmax"
   i = ZZ(floor(tmin*256))
   i1 = ZZ(floor(tmax.nextbelow()*256))
   assert i==i1,"i==i1"
   assert i in [255,256]
   t = RIF(tmin,tmax)
   r = RR(INVERSE[i-181],16)
   assert r==1, "r==1"
   l1 = RR(LOG_INV[i-181][0],16)
   assert l1==0, "l1==0"
   l2 = RR(LOG_INV[i-181][1],16)
   assert l2==0, "l2==0"
   err7 = 0
   # z = __builtin_fma(r,t,-1) -> z = t-1
   # z = RIF(r)*t-RIF(1)
   z = t-RIF(1)
   if verbose:
      print ("z=", z.lower(), z.upper())
   # we know |z| <= 0.0040283203125
   zmax = 0.0040283203125
   assert z.abs().upper()<=zmax,"z.abs().upper()<=zmax"
   err8 = 0
   # th = __builtin_fma (E, LOG2_H, l1) -> th = 0
   th = RIF(0)
   # tl = __builtin_fma (E, LOG2_L, l2) -> tl = 0
   tl = RIF(0)
   err1 = 0
   # fast_sum (hi, lo, th, z, tl) -> hi=z, lo=0
   hi = z
   lo = RIF(0)
   err2 = 0
   err3 = 0
   # p_1 (&ph, &pl, z)
   err0, ph, pl = analyze_p1(z.lower(),z.upper(),verbose=false)
   if verbose:
      print ("ph=", ph.lower(), ph.upper())
   if verbose:
      print ("pl=", pl.lower(), pl.upper())
   log1pz = z+ph+pl
   assert err0 <= 2^-67.27*log1pz.abs().upper(),"err0 <= 2^-67.27*log1pz.abs().upper()"
   assert log1pz.abs().upper()<0.0041,"log1pz.abs().upper()<0.0041"
   # fast_sum (hi, lo, *hi, ph, *lo + pl) -> fast_sum (hi, lo, z, ph, pl)
   #    fast_two_sum (hi, lo, z, ph)
   hi = hi + ph
   if z.abs().lower() >= RR("0x1.6a87dc9fb3cfcp-53",16):
      err4 = 0
   else:
      err4 = 2^-105*hi.abs().upper()
   if verbose:
      print ("err4:", log(err4)/log(2.))
   tmp = lo + pl
   err5 = 0
   u = RIFulp(hi)
   lo = RIF(-u,u)
   #    lo += lo_in + pl
   lo += tmp
   err6 = RIFulp(lo)
   if verbose:
      print ("err6:", log(err6)/log(2.))
   assert err4+err6<=2^-77.99, "err4+err6<=2^-77.99"
   if verbose:
      print ("err0:", log(err0)/log(2.))
   # fast_two_sum (hi, lo, *hi, *lo)
   hi_in = hi
   hi = hi_in+lo
   u = RIFulp(hi)
   lo = RIF(-u,u)
   err9 = 2^-105*hi.abs().upper()
   if verbose:
      print ("err9:", log(err9)/log(2.))
   err = err0+err1+err2+err3+err4+err5+err6+err7+err8+err9
   if verbose:
      print ("err:", log(err)/log(2.))
   assert err<=2^-74.87, "err<=2^-74.87"
   if rel==true:
      if verbose:
         print ("lower(hi+lo)=", (hi+lo).abs().lower())
      err = err/(hi+lo).abs().lower()
   return err, hi.abs().upper(), lo.abs().upper()

# analyze_log1_cancel2_all(k=8,rel=true)
# i= 256 j= 255 tmin= 0x1.00ffp+0 tmax= 0x1.01p+0 err= -67.3111475990597
# (5.46167093140910e-21, 0.00391389932113634, 8.67361737988404e-19)
# this routine is used in the error analysis of log_1()
def analyze_log1_cancel2_all(k=0,rel=false):
   maxerr = 0
   maxhi = 0
   maxlo = 0
   for i in [255,256]:
      for j in range(2^k):
         tmin = RR((i+j/2^k)/256)
         tmax = RR((i+(j+1)/2^k)/256)
         if rel and (tmin==1 or tmax==1):
            continue
         err, hi, lo = analyze_log1_cancel2(tmin,tmax,rel=rel,verbose=false)
         if err>maxerr:
            print ("i=", i, "j=", j, "tmin=", get_hex(tmin), "tmax=", get_hex(tmax), "err=", log(err)/log(2.))
            maxerr = err
         if hi>maxhi:
            # print ("i=", i, "j=", j, "tmin=", get_hex(tmin), "tmax=", get_hex(tmax), "hi=", hi)
            maxhi = hi
         if lo>maxlo:
            maxlo = lo
   return maxerr,maxhi,maxlo

# emulate log_1(x) for case 255/256 <= x < 257/256
# h,l = log1_case2(RR("0x1.00e8p+0",16),true)
# err0= -79.4032704164133
# err4= -infinity
# err6= -infinity
# h,l = log1_case2(RealField(53,rnd='RNDZ')("0x1.00e8p+0",16),true)
# err0= -76.7502081335451
# err4= -infinity
# err6= -infinity
# this internal routine is used in analyze_log1_case2()
def log1_case2(x,verbose=false):
   assert 255/256 <= x < 257/256, "255/256 <= x < 257/256"
   z = x-1
   # p_1 (&ph, &pl, z)
   ph,pl = p_1(z)
   if verbose:
      Z = z.exact_rational()
      Ph = ph.exact_rational()
      Pl = pl.exact_rational()
      err0 = abs(n(log(1+Z)-Z-Ph-Pl,200))
      print ("err0=", log(err0)/log(2.))
   # fast_two_sum (hi, lo, z, ph)
   hi = z + ph
   t = hi - z
   lo = ph - t
   if verbose:
      err4 = abs(n(hi.exact_rational()+lo.exact_rational()-(Z+Ph),200))
      print ("err4=", log(err4)/log(2.))
   # lo += pl
   lo0 = lo
   lo = lo + pl
   if verbose:
      err6 = abs(n(lo.exact_rational()-lo0.exact_rational()-Pl,200))
      print ("err6=", log(err6)/log(2.))
   return hi, lo

# return the maximal relative error for 255/256 <= x < 257/256
# analyze_log1_case2(RR(255/256))
# 1.5031534918165811740732517352363617273572185219408665982552e-21 # -69.17
# analyze_log1_case2(RR(257/256).nextbelow())
# 2.3958550839025528038399682692693128161410595502558839836038e-21 # -68.49
# this internal routine is used in analyze_log1_tiny()
def analyze_log1_case2(x,verbose=false):
   maxerr = 0
   for r in 'NZUD':
      R = RealField(53,rnd='RND'+r)
      hi, lo = log1_case2(R(x))
      X = x.exact_rational()
      y = hi.exact_rational()+lo.exact_rational()
      err = abs(n(y/log(X)-1,200))
      if verbose:
         print ('RND'+r, err)
      if err>maxerr:
         maxerr = err
   return maxerr

# analyze_log1_tiny()
# 2.0543252740130514283436938856434706338455523800942191966397e-32
# this routine is used to determine the maximal relative error in the
# case 255/256 <= x < 257/256 (E=0 and i in {255,256}) for
# |z| <= 1.57222633437748e-16, i.e., when fast_two_sum (hi, lo, z, ph)
# might be inexact
def analyze_log1_tiny():
   x = RR(1).nextbelow()
   return analyze_log1_case2(x)

# returns the maximal exponent difference between z and ph
# exp_dif_z_ph(0.0004)
# 12
# this internal routine is used in max_exp_dif_z_ph()
def exp_dif_z_ph(z):
   Z = z.exact_rational()
   R = RealField(200)
   t = R(log(1+Z))
   # we know that ph+pl has a maximal relative error < 2^-67.55 wrt log(1+z)
   u = abs(t*(1-R(2^-67.55))-z)
   # u is the smallest possible value of ph+pl
   # we know that |pl| < 0.00272*ph
   v = u / R(1+0.00272)
   # v is the smallest possible value of ph
   if v <= 0:
      return infinity
   ez = RR(z).sign_mantissa_exponent()[2]
   eph = RR(v).sign_mantissa_exponent()[2]
   return ez - eph

# returns the smallest z with exponent difference between z and ph <= 53
# max_exp_dif_z_ph()
# 1.57013874261013e-16
# this routine is used in the error analysis of log_1() (case E=0 and
# i in {255,256} thus 255/256 <= x < 257/256), to find the maximal absolute
# value of z such that fast_two_sum (hi, lo, z, ph) might be inexact
def max_exp_dif_z_ph():
   z = RR(0.0040283203125)
   while exp_dif_z_ph(z/2)<=53 and exp_dif_z_ph(-z/2)<=53:
      z = z/2
   a = z
   b = z/2
   # invariant: max(exp_dif_z_ph(+/-a)) <= 53 < max(exp_dif_z_ph(+/-b))
   while true:
      c = (a+b)/2
      if c==a or c==b:
         return a
      if max(exp_dif_z_ph(c),exp_dif_z_ph(-c))<=53:
         a = c
      else:
         b = c

# check Claude-Pierre's proof of Lemma 3
# sage: proof_lemma3_cpj()
# 92 -7.15450994905563
# 93 -7.90724285908015
# 94 -7.95560588064160
# 95 -7.17982103758484
# 187 [94] -7.95560588064160
# 76 -7.14201900487243
# 77 -7.93391080954223
# 78 -7.96657699846260
# 79 -7.15764965658622
# 196 [78] -7.96657699846260
# 71 -7.14513161673976
# 72 -7.95560588064155
# 73 -7.95560588064160
# 74 -7.14201900487246
# 199 [73] -7.95560588064160
def proof_lemma3_cpj():
   # case i <= 255
   for i in range(181,256):
      tmin = i/2^8
      tmax = (i+1)/2^8
      rmin = floor(2^8/tmax)/2^8
      rmax = ceil(2^8/tmin)/2^8
      kmin = (rmin-1)*2^8
      kmax = (rmax-1)*2^8
      assert kmin in ZZ, "kmin in ZZ"
      assert kmax in ZZ, "kmax in ZZ"
      d = kmax-kmin
      assert d<=3, "d<=3"
      Bi = infinity
      ki = []
      for k in [kmin..kmax]:
         r = 1+k/2^8
         tmin = i/2^8
         tmax = i/2^8+(2^45-1)/2^53
         zmin = abs(r*tmin-1)
         zmax = abs(r*tmax-1)
         Bik = max(zmin,zmax)
         if i in [187,196,199]:
            print (k,n(log(Bik)/log(2.)))
         if Bik<Bi:
            Bi = Bik
            ki = [k]
         elif Bik==Bi:
            ki.append(k)
      if not i in [187,196,199]:
         assert Bi<=2^-8,"case1: Bi<=2^-8"
      else:
         print (i,ki,n(log(Bi)/log(2.)))
   # case i > 255
   maxd = 0
   for i in range(256,363):
      tmin = i/2^8
      tmax = (i+1)/2^8
      rmin = floor(2^9/tmax)/2^9
      rmax = ceil(2^9/tmin)/2^9
      kmin = (rmin-1)*2^9
      kmax = (rmax-1)*2^9
      assert kmin in ZZ, "kmin in ZZ"
      assert kmax in ZZ, "kmax in ZZ"
      assert kmin<=kmax, "kmin<=kmax"
      d = kmax-kmin
      assert d<=3, "d<=3"
      Bi = infinity
      ki = []
      for k in [kmin..kmax]:
         r = 1+k/2^9
         tmin = i/2^8
         tmax = i/2^8+(2^44-1)/2^52
         zmin = abs(r*tmin-1)
         zmax = abs(r*tmax-1)
         Bik = max(zmin,zmax)
         if Bik<Bi:
            Bi = Bik
            ki = [k]
         elif Bik==Bi:
            ki.append(k)
      assert Bi<=2^-8,"case2: Bi<=2^-8"

# check_l1()
# i= 207 mindiff= 3.6507188831790577104487320958173892189931190886870746379761e-16
# i= 186 maxdiff= 1.1259274624680828133884734063645665358656134231149442094591e-13
def check_l1():
   mindiff = infinity
   maxdiff = 0
   for i in [181..262]:
      r = RR(INVERSE[i-181],16)
      R = r.exact_rational()
      l1 = RR(LOG_INV[i-181][0],16)
      L1 = l1.exact_rational()
      if R!=1:
         e = abs(n(-log(R)-L1,200))
         if e < mindiff:
            mindiff = e
            print ("i=", i, "mindiff=", e)
         if e > maxdiff:
            maxdiff = e
            print ("i=", i, "maxdiff=", e)
   assert 2^-52 < mindiff, "2^-52 < mindiff"
   assert maxdiff < 2^-43, "maxdiff < 2^-43"
      
