@rnd = float<ieee_64,RND>;

r = TEMPR;
l1 = TEMPL1;
l2 = TEMPL2;
E = TEMPE;

z = r*t-1;
LOG2_H = 0x1.62e42fefa38p-1;
LOG2_L = 0x1.ef35793c7673p-45;

# th = __builtin_fma (E, LOG2_H, l1)
th = E*LOG2_H+l1;

# tl = __builtin_fma (E, LOG2_L, l2)
tl rnd= fma (E, LOG2_L, l2);
Tl = E*LOG2_L+l2;
err1 = tl - Tl;
# err1 is non-zero for example for t=0x1.6a09e667f3bcdp-1, E=-1074 and RNDZ
# where err1=1.2274675685238944e-26

# fast_sum (hi, lo, th, z, tl)
# this decomposes into:
#    fast_two_sum (hi, lo, th, z)
#    lo += tl
# thus:
#    hi = th + z
#    e = hi - th
#    lo = z - e
#    lo += tl
hi rnd= th + z;
e rnd= hi - th;
lo rnd= z - e;
# err2 is the error in fast_two_sum(hi, lo, th, z)
# it should be zero
err2 = (hi + lo) - (th + z);
lo2 rnd= lo + tl;
# err3 is the error in lo += tl
# err3 is non-zero for example for t=0x1.478f2c637ea98p+0, E=-1057 and RNDN
# where err3=-6.462348535570529e-27
Lo2 = lo + tl;
err3 = lo2 - Lo2;

# fast_sum (hi2, lo3, hi, ph, lo2 + pl)
# this decomposes into:
#    fast_two_sum (hi2, lo3, hi, ph)
#    lo3 += lo2 + pl
# thus:
#    hi2 = hi + ph
#    e2 = hi2 - hi
#    lo3 = ph - e2
#    lo3 += lo2 + pl
hi2 rnd= hi + ph;
e2 rnd= hi2 - hi;
lo3 rnd= ph - e2;
# err4 is the error in fast_two_sum (hi2, lo3, hi, ph)
err4 = (hi2 + lo3) - (hi + ph);
# err4 is nonzero for example for t=0x1.767dce434a9b1p-1, E=1 and RNDZ
# where err4=-6.1625996638468916e-33
tmp rnd= lo2 + pl;
# err5 is the error in lo2 + pl
Tmp = lo2 + pl;
err5 = tmp-Tmp;
# err5 is nonzero for example for t=0x1.6a09e667f3bcdp-1, E=-1074 and RNDZ
# where err5=1.3183191012563879e-24
# err6 is the error in lo3 += tmp
lo4 rnd= lo3 + tmp;
Lo4 = lo3 + tmp;
err6 = lo4-Lo4;
# err6 is nonzero for example for t=0x1.767dce434a9b1p-1, E=1 and RNDZ
# where err6=6.162975822039155e-33

# approximation error between log(2) and LOG2_H+LOG2_L
# e8 = LOG2_H+LOG2_L-log(2): -1.95e-30 <= e8 <= -1.94e-30
err8 = e8 * E;

{
    @FLT(t,53) /\ t in [T0,T1] ->
    rnd(z) = z ->
    |z| <= 0.0040283203125 ->
    rnd(th) = th ->
    |z/th| in [-1,1] -> # we should have |z| <= |th| in fast_two_sum
    err2 = 0 ->         # err2 is zero since exp(th)-exp(z) <= 53
    |ph| <= 8.2e-6 ->
    |pl| <= 2.2e-8 ->
    |err0| <= 1.4246e-23 -> # maximal error |ph + pl - (log(1+z)-z)| 2^-72.1854
    |hi2 + lo3 -/ hi + ph| <= 1b-104 ->
    |err7| <= 1b-97 -> # approximation error between -log(r) and l1+l2
    e8 in [-1.95e-31,-1.94e-31] ->
    err0+err1+err3+err4+err5+err6+err7+err8 in ?
}

lo -> (hi + lo - (th + z)) - (hi - (th + z));
lo3 -> (hi2 + lo3 - (hi + ph)) - (hi2 - (hi + ph));
