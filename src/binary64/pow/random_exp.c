#include "fenv_mpfr.h"
#include "pow.c"

#include <stdlib.h>
#include <string.h>
#include <time.h>

double cr_exp(double x) {
  double hi, lo, ex;
  exp_1(&hi, &lo, &ex, x, 0);

  return (hi + lo) * ex;
};

/* reference code using MPFR */
double ref_exp(double x) {
  mpfr_t z, _x;
  mpfr_inits2(53, z, _x, NULL);
  mpfr_set_d(_x, x, MPFR_RNDN);
  int inex = mpfr_exp(z, _x, rnd2[rnd]);
  mpfr_subnormalize(z, inex, rnd2[rnd]);
  double ret = mpfr_get_d(z, MPFR_RNDN);
  mpfr_clears(z, _x, NULL);
  return ret;
}

int main(int argc, char *argv[]) {
  srand(time(NULL));

  double range = 0x1.6232bdd7abcd2p+9 + 0x1.62e42fefa39efp+9;
  double div = RAND_MAX / range;

  long errors = 0;
  long valid = 0;

  long n = 0x989680;
  long freq = 20;

  if (argc >= 3 && strcmp(argv[1], "-n") == 0) {
    n = strtol(argv[2], NULL, 0);
    argv += 2;
  }

  printf("Errors :\n");

  for (long i = 1; i < n; i++) {
    double x = rand() / div - 0x1.6232bdd7abcd2p+9;

    double z = cr_exp(x);
    double z_ref = ref_exp(x);

    if (__builtin_isfinite(z_ref)) {
      valid++;
      if (z != z_ref) {
        // if (__builtin_isfinite(z))
        printf("    incorrect : %la - %la %la\n", x, z, z_ref);

        errors++;
      }
    }

    if (!(i % (n / freq))) {
      printf("  %ld / %ld - %la\n", errors, valid, (double)errors / valid);
    }
  }

  return 0;
}
