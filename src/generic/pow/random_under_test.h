/* sample x,y in [0,20] by default */

#ifndef PERF_XMIN
#define PERF_XMIN 0
#endif
#ifndef PERF_XMAX
#define PERF_XMAX 20
#endif

#ifndef PERF_YMIN
#define PERF_YMIN 0
#endif
#ifndef PERF_YMAX
#define PERF_YMAX 20
#endif

static inline TYPE_UNDER_TEST random_under_test(void) {
  static int count = 0;
  /* for bivariate functions, we generate alternatively x[i] and y[i] */
  if ((count++ & 1) == 0)
    return PERF_XMIN + (PERF_XMAX - PERF_XMIN) * ((TYPE_UNDER_TEST)rand() / (TYPE_UNDER_TEST)RAND_MAX);
  else
    return PERF_YMIN + (PERF_YMAX - PERF_YMIN) * ((TYPE_UNDER_TEST)rand() / (TYPE_UNDER_TEST)RAND_MAX);
}
