This is source code corresponding to the article
"Towards a correctly-rounded and fast power function in binary64 arithmetic"
by Tom Hubrecht, Claude-Pierre Jeannerod and Paul Zimmermann,
proceedings of the 30th IEEE International Symposium on Computer Arithmetic
(ARITH 2023).
